# Dummy account for login credentials

|           Email             |             Password              |               user-type                |                    
| --------------------------- | --------------------------------- | -------------------------------------  | 
| admin@gasofttech.com        |     chandan                       | CA (not required handled via backend)  |


# Dummy FCM token

{

    fa2TmeTKQzu8ByvVsEYPTY:APA91bE5aM3--DxXNFiKAmtOGkzICoH1q-OGPzyrB-G-1jksDNsxhV2lDO8VRMNQduKuTyWPpNduQuYDPrFAj-xqMZHGV7COmGWZ7L243hiir1YsKhM8y2csw261RE1GRNmcXScPMJPf

}


BASE URL : "http://localhost:3000"


# API Documentation For Mobile App
=========================================================

METHOD: GET

URL: http://localhost:3000/site/get-available-site

DESC: To Fetched the details of all the available site

=========================================================

METHOD: POST

URL: http://localhost:3000/site/view-site-summary

KEY: 

{

    "site_id": 1001

}

DESC: To get the complete site summary information

=========================================================

METHOD: POST

URL: http://localhost:3000/site/get-user-sites

KEY: 

{
    "user_id":101
}

DESC: Get user sites information on user_id

=========================================================

METHOD: GET

URL: http://localhost:3000/site/get-organizations

DESC: To get all the available site_id and site_name

=========================================================

METHOD: POST

URL: http://localhost:3000/login

KEY: 

{

    "email": "admin@gasofttech.com"

    "user_password":"chandan"

}

DESC: For login into mobile app 

=========================================================

METHOD: POST

URL: http://localhost:3000/register

KEY: {

    "form_id":108,

    "company_name":"TOMCAT",

    "alloted_organization":[{"site_id":51,"site_name":"C R JAIN CHARITABLE TRUST","ca_site_no":"123456","site_layout_name":"fixed layout","site_dimension":"12*56*46","purpose":"site-vist"},{"site_id":313,"site_name":"KARNATAKA DIARY DEVELOPMENT CORPORATION LIMITED","ca_site_no":"123456","site_layout_name":"fixed layout","site_dimension":"12*56*46","purpose":"site-vist"}],

    "contact_no":"9686711048",

    "email":"kumark.kr@powerguru.co.in",

    "user_password":"chandan",

    "company_address":"vibhuti khand",

    "token":"adfdsrerfsd!$%$%$^$dfdgdfgf4645dfdgd",

    "registration_doc":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z8BQDwAEhQGAhKmMIQAAAABJRU5ErkJggg=="

}

DESC: To register new users from mobile app

=========================================================

METHOD: GET

URL: http://localhost:3000/get-user-id

DESC: For generating new form_id

=========================================================

METHOD : POST

URL : http://localhost:3000/send-otp

KEY : 

{

    "phoneNumber": "9686711048"

}

DESC: To send OTP

=========================================================

METHOD : POST

URL : http://localhost:3000/retry-otp

KEY : 

{

    "phoneNumber": "9686711048"

}

DESC: To resend the OTP

=========================================================

METHOD : POST

URL : http://localhost:3000/verify-otp

KEY : 

{

    "phoneNumber": "9686711048",

    "otp": 8176

}

DESC: To verify the OTP

=========================================================

METHOD : POST

URL : http://localhost:3005/purchase/create-order

KEY : 

{

   	"user_id":1598194344668,
	"authorized_person":"chandan singh",
	"site_id":1075,
	"resolution":"resolution"

}

DESC: To Purchase an assets/organization
