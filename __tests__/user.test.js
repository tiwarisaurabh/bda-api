const request = require('supertest')
const faker = require('faker')
const app = require('../app')

jest.setTimeout(30000);

afterAll(async () => {
	await new Promise(resolve => setTimeout(() => resolve(), 500)); 
});

describe('--- User Tests ---', () => {
    let userID;
    let email;
    let password;
    it('should generate user ID', async () => {
        const res = await request(app)
            .get('/get-user-id')
            .send()
        expect(res.statusCode).toEqual(200);
        userID = res.body.data;
        expect(res.body.status).toBeTruthy();
    })

    it('should register user', async () => {
        email = faker.internet.email()
        password = faker.internet.password()
        const res = await request(app)
            .post('/register')
            .send({
                user_id: userID,
                fname: faker.name.firstName(),
                lname: faker.name.lastName(),
                user_password: password,
                gender: "M",
                email: email,
                contact_no: faker.phone.phoneNumber(),
                address: faker.address.streetAddress(),
                user_role: 'S',
                status: 'Y',
                created_by: 10001,
                updated_by: 10001
            })
        expect(res.statusCode).toEqual(200);
        expect(res.body.status).toBeTruthy();
    })

    it('should login user', async () => {
        const res = await request(app)
            .post('/login')
            .send({
                email: email,
                user_password: password
            })
        expect(res.statusCode).toEqual(200);
        expect(res.body.data).toBeTruthy();
    })
})