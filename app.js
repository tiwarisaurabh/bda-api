var createError = require('http-errors');
var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var bodyParser = require('body-parser');
var path = require('path')

var indexRouter = require('./routes/index');
var siteRouter = require('./routes/siteDetails');
var registrationRouter = require('./routes/pendingUser');
var dashboardRouter = require('./routes/dashboard');
var purchaseRouter = require('./routes/purchase');
var editInstitutionRouter = require('./routes/institutionEdit');
var additionalSiteRouter = require('./routes/newSiteReq')
var institutionRegistration = require('./routes/institutionRegistration');
var customSiteRouter = require('./routes/addCustomSite')
var reportRouter = require('./routes/report')


var app = express();

app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({limit: '100mb', extended: true }));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors())
app.use('/static', express.static(path.join(__dirname, 'document')));

app.use('/', indexRouter);
app.use('/site',siteRouter);
app.use('/user',registrationRouter);
app.use('/dashboard',dashboardRouter);
app.use('/purchase',purchaseRouter);
app.use('/edit',editInstitutionRouter);
app.use('/additional-site',additionalSiteRouter)
app.use('/institution-registration',institutionRegistration)
app.use('/custom-site',customSiteRouter);
app.use('/report',reportRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send({
    error: err
  });
});

module.exports = app;
