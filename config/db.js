'use strict';

/*Database credentials*/

const promise = require('bluebird');

const options = {
    promiseLib: promise,
    capSQL: false
};

const pgp = require('pg-promise')(options);

//Production connection
// const conString = {
//     database: 'bda_db',
//     port: 5432,
//     host: '157.245.100.36',
//     user: 'bdaadmin',
//     password: '@Bdaadmin#123',
//     poolSize: 50,
//     poolIdleTimeout: 10000
// };

//Testing connection
const conString = {
    database: 'BDA_DB',
    port: 5432,
    host: '172.105.41.145',
    user: 'automation',
    password: 'Automation@123',
    poolSize: 50,
    poolIdleTimeout: 10000
};


const db = pgp(conString);

module.exports = {
    db: db
};