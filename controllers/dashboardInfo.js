"use strict";

const Dashboard = require('../models/dashboard-info');

module.exports = {

    //Total number of sites which are enabled
    totalSites: async () => {
        let total = await Dashboard.getTotalSites();
        if(!total.status) return {
            status: false,
            data: null,
            message: total.message
        }

        return total;
    },

    //Total number of users which are enabled
    totalCompany: async () => {
        let total = await Dashboard.getTotalCompany();
        if(!total.status) return {
            status: false,
            data: null,
            message: total.message
        }
        
        return total;
    },
    
    //Total number of bdaEmployee
    bdaEmployee: async () => {
        let bdaEmployee = await Dashboard.totalBdaEmployee();
        if(!bdaEmployee.status) return {
            status: false,
            data: null,
            message: bdaEmployee.message
        }

        return bdaEmployee;
    },

    _totalInstitutionSite:async (input) => {
        let total = await Dashboard.totalInstitutionSite(input)
        if(!total.status){
            return {
                status:false,
                data:null,
                message:total.message
            }
        }

        return total;
    }
}
