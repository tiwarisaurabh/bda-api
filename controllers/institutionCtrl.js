"use strict"

const EditInstitution = require('../models/institution-edit');

module.exports = {

    //Institution change password
    changePassword: async(input) => {
        let institutionPassword = await EditInstitution.updateInstitutionChangePassword(input)
        if(!institutionPassword) return {
            status: false,
            data: null,
            message:institutionPassword.message
        }

        return institutionPassword
    },
    
    //Institution update profile
    updateProfile: async(input) => {
        let institutionProfile = await EditInstitution.updateInstitutionProfile(input)
        if(!institutionProfile) return {
            status: false,
            data: null,
            message:institutionProfile.message
        }

        return institutionProfile
    },

    //Institution view profile
    viewProfile: async(input) => {
        let profileDetails = await EditInstitution.viewInstitutionProfile(input)
        if(!profileDetails) return {
            status: false,
            data: null,
            message:profileDetails.message
        }

        return profileDetails
    }
    

}
