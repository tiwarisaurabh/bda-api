"use strict"

const InstitutionRegistration = require('../models/institution-Registration');

module.exports = {
    
    _uploadRegistrationCertificate:async(input) => {
        let registrationUpload = await InstitutionRegistration.uploadRegistrationCertificate(input);
        if(!registrationUpload) return {
            status:false,
            data:null,
            message:registrationUpload.message
        }
    
        return registrationUpload
    },

    _deleteUploadedCertificate: async (input) => {
        let document = await InstitutionRegistration.deleteUploadedCertificate(input)
        if(!document) return{
            status:false,
            data:null,
            message:document.message
        }

        return document
    },
    
    _getRegistrationUploadedFile:async(input) => {
        let getRegistrationUpload = await InstitutionRegistration.getRegistrationUploadedFile(input);
        if(!getRegistrationUpload) return {
            status:false,
            data: null,
            message:getRegistrationUpload.message
        }
    
        return getRegistrationUpload
    },

    _insertRegistrationDetailsTemp:async(input) => {
        let insertRegistration = await InstitutionRegistration.insertRegistrationDetailsTemp(input)
        if(!insertRegistration) return {
            status:false,
            data: null,
            message:insertRegistration.message
        }

        return insertRegistration
    },

    _institutionRegistration:async (input) => {
        let registration = await InstitutionRegistration.institutionRegistration(input)
        if(!registration) return {
            status:false,
            data:null,
            message:registration.message
        }
        return registration
    },

    _checkUserExist:async (input) => {
        let check = await InstitutionRegistration.checkUserExist(input)
        if(!check) return {
            status:false,
            data:null,
            message:check.message
        }
        return check
    }


}
