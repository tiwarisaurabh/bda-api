const Users = require('../models/user');

module.exports = {

    //Service for mobile app login
    login: async (input) => {
        let user = await Users.login(input);

        if (!user.status) return {
            status: false,
            data: null,
            message: user.message
        }

        return user;
    },

    //Service for user registration through mobile app
    register: async (input) => {
        let user = await Users.registerUser(input);
        
        if (!user.status) return {
            status: false,
            data: null,
            message: user.message
        }
        console.log('****registration*****',user)
        return user;
    },

    //Service for generating form_id i.e user_id once approved form web-portal
    generateUserID: async () => {
        let user = await Users.createUserID();
        if (!user.status) return {
            status: false,
            data: null,
            message: user.message
        }

        return user;
    },

    //Service for employee regesitration for web
    registerEmployee: async (input) => {
        let user = await Users.registerEmployee(input);
        
        if (!user.status) return {
            status: 400,
            data: null,
            message: user.message
        }

        return user;
    },

    //Service for employee login on web-portal
    loginEmployee: async (input) => {
        let user = await Users.loginEmployee(input);

        if (!user.status) return {
            status: 400,
            data: null,
            message: user.message
        }

        return user;
    },

    //Service for sending OTP for mobile app
    sendOTP: async (phoneNumber) => {
        const countryCode = '91';
        const mobileNumber = phoneNumber;
        const completeMobileNumber = `${countryCode}${mobileNumber}`;
        let Phone = await Users.sendOtpToMobile(completeMobileNumber)

        if (!Phone.status) return {
            status: false,
            data: null,
            message: Phone.message
        }

        return Phone;
    },

    //Service for verify OTP for mobile app
    verifyOTP: async (phoneNumber, otp) => {
        const countryCode = '91';
        const mobileNumber = phoneNumber;
        const completeMobileNumber = `${countryCode}${mobileNumber}`; 

        let Phone = await Users.verifyOtpSentToMobile(completeMobileNumber, otp)

        if (!Phone.status) return {
            status: false,
            data: null,
            message: Phone.message
        }

        return Phone;
    },

    //Service for resend the OTP for mobile app
    retryOTP: async (phoneNumber) => {
        const countryCode = '91';
        const mobileNumber = phoneNumber;
        const completeMobileNumber = `${countryCode}${mobileNumber}`;

        let Phone = await Users.retryOtpSend(completeMobileNumber)

        if (!Phone.status) return {
            status: false,
            data: null,
            message: Phone.message
        }

        return Phone;
    },

    //Service for updating bda employee profile
    editBdaEmpProfile:async (input) => {
        let profileChanged = await Users.updateBdaEmpProfile(input);
        if(!profileChanged.status) return {
            status: 400,
            data: null,
            message: profileChanged.message
        }

        return profileChanged;
    },

    //Service for setting new password for bda employee
    changePasswordBDAEmp:async (input) => {
        let passwordChanged = await Users.newPasswordBDAEmp(input);
        if(!passwordChanged.status) return {
            status:400,
            data: null,
            message: passwordChanged.message
        }

        return passwordChanged
    },

    //To get bda employee list
    bdaEmployeeList: async() => {
        let employee = await Users.getAllBDAEmployees();
        if(!employee.status) return {
            status:400,
            data: null,
            message:employee.message
        }

        return employee
    },

    //check duplicate email count
    checkUser:async(input) => {
        let user = await Users.checkUserExistOnRegister(input);
        if(!user.status) return {
            status: false,
            data: null,
            message:user.message
        }

        return user
    },

    //Forgot password for bda
    _forgotPasswordBDA:async(input) => {
        let userPassword = await Users.forgotPasswordBDA(input);
        if(!userPassword) return {
            status: false,
            data: null,
            message:userPassword.message
        }

        return userPassword
    },

    //resetPassword for bda
    _BdaResetPasswordLink:async(input) => {
        let resetPassword = await Users.BdaResetPasswordLink(input)
        if(!resetPassword) return {
            status:false,
            data:null,
            message:resetPassword.message
        }
        return resetPassword
    },

    //Forgot password for institution
    _forgotPasswordInstitution: async(input) => {
        let userPassword = await Users.forgotPasswordInstitution(input);
        if(!userPassword) return {
            status:false,
            data:null,
            message:userPassword.message
        }

        return userPassword
    },

    //Institution Reset Password Link
    _InstitutionResetPasswordLink: async(input) => {
        let resetPassword = await Users.InstitutionResetPasswordLink(input);
        if(!resetPassword) return {
            status:false,
            data:null,
            message:resetPassword.message
        }

        return resetPassword
    }
}   