"use strict"

const NewSiteReqModule = require('../models/new-site-req')

module.exports = {

    //Add new site request
    _addNewSite:async(input) => {
        let site = await NewSiteReqModule.addNewSite(input)
        if(!site.status) return {
            status:false,
            data:null,
            message:site.message
        }

        return site
    },

    //Approve new site request
    _approveNewSiteReq: async(input) => {
        let approveSite = await NewSiteReqModule.approveNewSiteReq(input)
        if(!approveSite.status) return {
            status:false,
            data:null,
            message:approveSite.message
        }

        return approveSite
    },

    //Get pending new site list
    _getPendingNewSite:async(input) => {
        let pendingNewSite = await NewSiteReqModule.getPendingNewSite()
        if(!pendingNewSite.status) return {
            status:false,
            data:null,
            message:pendingNewSite.message
        }

        return pendingNewSite
    },

    _rejectPendingNewSite:async(input) => {
        let rejectSite = await NewSiteReqModule.rejectPendingNewSite(input)
        if(!rejectSite.status) return {
            status:false,
            data:null,
            message:rejectSite.message
        }

        return rejectSite
    },

    _storeNewSiteRequestAsTemp:async(input) => {
        let userSite = await NewSiteReqModule.storeNewSiteRequestAsTemp(input);
        if(!userSite) return {
            status: false,
            data: null,
            message: userSite.message
        }
        return userSite;
    },

    _getTempNewSiteReq:async(input) => {
        let getPendingNewSite = await NewSiteReqModule.getTempNewSiteReq(input);
        if(!getPendingNewSite) return{
            status: false,
            data: null,
            message: getPendingNewSite.message
        }
        return getPendingNewSite
    },

    _uploadRegistrationCertificate: async (input) => {
        let upload = await NewSiteReqModule.uploadRegistrationCertificate(input)
        if(!upload) return {
            status:false,
            data:null,
            message:upload.message
        }

        return upload
    },

    _institutionRegistration:async (input) => {
        let register = await NewSiteReqModule.institutionRegistration(input)
        if(!register) return {
            status:false,
            data:null,
            message:register.message
        }

        return register
    },

    _deleteUploadedCertificate: async (input) => {
        let document = await NewSiteReqModule.deleteUploadedCertificate(input)
        if(!document) return{
            status:false,
            data:null,
            message:document.message
        }

        return document
    },
}