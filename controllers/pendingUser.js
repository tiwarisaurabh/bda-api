'use strict';

const PendingUserModule = require('../models/pending-user');

module.exports = {

    //Service to get the pending user details for web-portal
    pendingUsers: async (input) => {
        let user = await PendingUserModule.getPendingUsers();
        if (!user.status) return {
            status: 400,
            data: null,
            message: user.message
        }

        return user;
    },

    //Service to approve the pending user for web-portal
    _approveInstitutionRegistration: async (input) => {
        let user = await PendingUserModule.approveInstitutionRegistration(input);
        if(!user.status) return {
            status: 400,
            data: null,
            message: user.message
        }

        return user;
    },

    //To reject the registration
    _rejectInstitutionRegistration:async (input) => {
        let reject = await PendingUserModule.rejectInstitutionRegistration(input);
        if(!reject.status) return {
            status:400,
            data: null,
            message:reject.message
        }

        return reject;
    },

    //Service to get the list of approved user details for web-portal
    getAllApprovedUsers: async (input) => {
        let user = await PendingUserModule.getApprovedUser();
        if(!user.status) return {
            status: 400,
            data: null,
            message: user.message
        }

        return user;
    },

    registrationStatus:async(input) => {
        let registrationStatus = await PendingUserModule.checkRegistrationStatus(input);
        if(!registrationStatus.status) return {
            status:false,
            data: null,
            message:registrationStatus.message
        }

        return registrationStatus
    }

}