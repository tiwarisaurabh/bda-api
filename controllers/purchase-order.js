"use strict";

const PurchaseOrder = require("../models/purchaseOrder");

module.exports = {
    
    //Service for generating purchase order for mobile app
    generatePurchaseOrder:async(input) => {
        let order = await PurchaseOrder.insertPurchaseOrder(input)
        if(!order.status) return {
            status: false,
            data: null,
            message:order.message
        }

        return order
    },

    //Service to get the purchase order details for web-portal
    getPurchaseItems: async(input) => {
        let order = await PurchaseOrder.getPurchaseOrderDetails()
        if(!order.status) return {
            status: 400,
            data: null,
            message:order.message
        }

        return order
    },

    getPendingPurchaseOrderDetails:async () => {
        let order = await PurchaseOrder.getPendingPurchaseOrderDetails()
        if(!order.status) return {
            status: false,
            data: null,
            message:order.message
        }

        return order
    },

    //To get institution purchase order details
    institutionPurchaseDetails:async(input) => {
        let order = await PurchaseOrder.getInstitutionPurchaseDetails(input)
        if(!order.status) return {
            status:false,
            data:null,
            message:order.message
        }

        return order
    },

    uploadSignedCopy:async(input) => {
        let order = await PurchaseOrder.postUploadSignedCopy(input)
        if(!order.status) return {
            status:false,
            data:null,
            message:order.message
        }

        return order
    },

    //Purchase order audit for web-portal
    purchaseOrderAudit:async(input) => {
        let auditReq = await PurchaseOrder.auditPurchaseOrder(input)
        if(!auditReq) return {
            status:false,
            data:null,
            message:auditReq.message
        }

        return auditReq
    },

    //Update the existing purchase order
    updatePurchaseReq:async(input) => {
        let updateReq = await PurchaseOrder.updatePurchaseOrder(input)
        if(!updateReq) return {
            status: false,
            data: null,
            message:updateReq.message
        }

        return updateReq
    },

    //Purchase timeline history
    institutionPurchaseTimline:async (input) => {
        let history = await PurchaseOrder.getInstitutionPurchaseTimline(input)
        if(!history) return {
            status: false,
            data: null,
            message:history.message
        }

        return history
    },

    purchaseTimline: async(input) => {
        let history = await PurchaseOrder.getBDAPurchaseTimeline(input)
        if(!history) return {
            status: false,
            data: null,
            message:history.message
        }

        return history
    },

    pendingInstitutionPurchaseList: async(input) => {
        let pendingPurchase = await PurchaseOrder.getPendingInstitutionPurchaseOrderDetails(input)
        if(!pendingPurchase) return {
            status: false,
            data: null,
            message:pendingPurchase.message
        }

        return pendingPurchase
    },

    purchaseOrderDtlMobile:async(input) => {
        let orderDtl = await PurchaseOrder.getInstitutionPurchaseDetailsMobile(input)
        if(!orderDtl) return {
            status: false,
            data: null,
            message:orderDtl.message
        }

        return orderDtl
    }
}