const Site = require('../models/site-details');

module.exports = {

    //Service to get the list of available site details for mobile app
    getSite: async (input) => {
        let site = await Site.siteDetails();

        if (!site.status) return {
            status: false,
            data: null,
            message: site.message
        }

        return site;
    },

    //Service to get the site dimensions details for mobile app on site id
    getSiteSummary: async (input) => {
        let site = await Site.siteSummaryDetails(input);
        if (!site.status) return {
            status: false,
            data: null,
            message: site.message
        }

        return site;
    },

    //Service to get the site id and site name for mobile app
    getAvailableSite: async () => {
        let site = await Site.getAvailableSite();
        if(!site.status) return {
            status: false,
            data: null,
            message: site.message
        }

        return site;
    },

    //Service to get the user site details for mobile app
    userSiteDetails:async (input) => {
        let userSite = await Site.getUserSiteDetails(input);
        if(!userSite) return {
            status: false,
            data: null,
            message: userSite.message
        }

        return userSite;
    },

    _getSiteMasterDataForUser:async(input) => {
        let userSite = await Site.getSiteMasterDataForUser(input);
        if(!userSite) return {
            status: false,
            data: null,
            message: userSite.message
        }

        return userSite;
    },

    _getNotScreeningPhaseSite:async(input) => {
        let userSite = await Site.getNotScreeningPhaseSite(input);
        if(!userSite) return {
            status: false,
            data: null,
            message: userSite.message
        }
        return userSite;
    }

}