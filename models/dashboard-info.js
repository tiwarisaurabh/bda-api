'use strict';

const db = require('../config/db').db;

module.exports  = {

    //Total number of sites which are enabled
    getTotalSites: async () => {
        try {
            let totalSites = await db.one('SELECT COUNT(DISTINCT SITE_ID) AS SITE_COUNT FROM BDA_SITE_HIERARCHY_MASTER WHERE STATUS = $1',['Y'])
            if(!totalSites) return {
                status: false,
                message: 'No Data Found!'
            }
            return {
                status: true,
                data: totalSites,
                message: 'Records fetched successfully!'
            }
        } catch (error) {
            console.log('========= getTotalSites ERR =======', error);
            return {
                status: false,
                message: 'Internal error! Please try again'
            }
        }
    },

    //Total number of users which are enabled
    getTotalCompany: async () => {
        try {
            let totalCompany = await db.tx(t => {
                const approvedCompany = db.one('SELECT COUNT (DISTINCT USER_ID) AS APPROVED_CNT FROM BDA_USER_PROFILE WHERE STATUS = $1',['Y']);
                const pendingCompany = db.one('SELECT COUNT(DISTINCT FORM_ID) AS PENDING_CNT FROM BDA_USER_REGISTRATION WHERE FORM_ID NOT IN(SELECT USER_ID FROM BDA_USER_PROFILE)');
                return t.batch([approvedCompany,pendingCompany])
            })
            if(!totalCompany.length) return {
                status:false,
                message:'No Data Found'
            }
            return {
                status: true,
                data: {
                    approvedCompanyCnt:totalCompany[0].approved_cnt,
                    pendingCompanyCnt:totalCompany[1].pending_cnt
                },
                message: 'Records fetched successfully!'
            }
        } catch (error) {
            console.log('========= getTotalApprovedCompany ERR =======', error);
            return {
                status: false,
                message: 'Internal error! Please try again'
            }
        }
    },

    //Total number of bdaEmployee
    totalBdaEmployee: async () => {
        try {
            let bdaEmployee = await db.one('SELECT COUNT(DISTINCT USER_ID) AS EMPLOYEE_CNT FROM BDA_EMPLOYEE_PROFILE WHERE STATUS = $1',['Y'])
            if(!bdaEmployee) return {
                status: false,
                message: 'No Data Found'
            }

            return {
                status: true,
                data: bdaEmployee,
                message: 'Records fetched successfully!'
            }
        } catch (error) {
            console.log('========= totalBdaEmployee ERR =======', error);
            return {
                status: false,
                message: 'Internal error! Please try again'
            }
        }
    },

    totalInstitutionSite:async(input) => {
        console.log('========== totalInstitutionSite Req ==========',input)
        try {
            let institutionSite = await db.one('select count(distinct registration_no) as site_cnt from bda_user_org_mapping where user_id = $1',[input.user_id])
            if(!institutionSite){
                return {
                    status:false,
                    message:'NoRecordFound'
                }
            }else{
                return {
                    status:true,
                    data:institutionSite,
                    message:'success'
                }
            }
        } catch (error) {
            console.log('========== totalInstitutionSite Err ==========',error)
            return {
                status:false,
                message:'InternalServerError'
            }
        }
    }

}