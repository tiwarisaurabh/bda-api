"use strict"
const db = require('../config/db').db;
const uploadFolder = require('../config/constant');
const docHelper = require('../utils/image-handler');
const fs = require('fs')
const passwordHandler = require('../utils/password-handler');
const Notification = require("../utils/fcm-notification");
const SendEmail = require('../utils/send-email');


module.exports = {
    uploadRegistrationCertificate: async (input) => {
        console.log('[====== Upload Registration Certificate  =======]', input.site_id,input.email)

        //For image handling
        if (input.file_type === 'pdf') {
            var decodedPdf = await docHelper.decodeBase64Pdf(input.upload_img);
            var imageBuffer = await decodedPdf.data;
            var type = await decodedPdf.type;
            var fileName = Date.now() + '.' + type;

        } else {
            var decodedImg = await docHelper.decodeBase64Image(input.upload_img);
            var imageBuffer = await decodedImg.data;
            var type = await decodedImg.type;
            var fileName = Date.now() + '.' + type;
        }

        //For uploading image
        try {
            fs.writeFileSync(uploadFolder.identityPath + fileName, imageBuffer, 'base64');
        } catch (err) {
            console.error(err)
            return {
                status: false,
                message: 'failed'
            }
        }
        try {
            let order = await db.one('UPDATE BDA_TEMP_REGISTRATION_DTLS SET REGISTRATION_DOC_ID = $1, REGISTRATION_DOC_NAME = $2, UPLOADED_ON = $3 WHERE EMAIL = $4 AND SITE_ID = $5 RETURNING SITE_ID',[fileName,input.registration_file_name,new Date(),input.email,input.site_id])
            if(!order) {
                return {
                    status: false,
                    message: 'failed'
                }
            }else{
                return {
                    status: true,
                    message: 'success'
                }
            }
            
        } catch (error) {
            console.log('[====== Upload Registration Certificate Error =======]', error)
            return {
                status: false,
                message: 'InternalError'
            }
        }
    },

    deleteUploadedCertificate:async (input) => {
        console.log('Delete registration certificate',input)
        try {
            let document = await db.one('UPDATE BDA_TEMP_REGISTRATION_DTLS SET REGISTRATION_DOC_ID = $1, REGISTRATION_DOC_NAME = $2 WHERE EMAIL = $3 AND SITE_ID = $4 RETURNING SITE_ID',[null,null,input.email,input.site_id])
            if(!document) {
                return {
                    status: false,
                    message: 'failed'
                }
            }else{
                return {
                    status: true,
                    message: 'success'
                }
            }
        } catch (error) {
            console.log('delete document error', error)
            return {
                status: false,
                message: 'InternalServerError'
            }
        }
    },

    getRegistrationUploadedFile: async (input) => {
        try {
            let uploadedFile = await db.any('SELECT * FROM BDA_TEMP_REGISTRATION_DTLS WHERE EMAIL = $1 ORDER BY SITE_ID DESC', [input.email.toLowerCase()])
            if (!uploadedFile) {
                return {
                    status: false,
                    message: 'NoRecordFound'
                }
            } else {
                return {
                    status: true,
                    data: uploadedFile,
                    message: 'Success'
                }
            }
        } catch (error) {
            console.log('getRegistrationUploadedFile', error)
            return {
                status: false,
                message: 'InternalServerError'
            }
        }
    },

    insertRegistrationDetailsTemp: async (input) => {
        console.log(input)
        try {
            let siteDetails = input.site_name;
            let mappedSiteDetail = await siteDetails.map(async (items) => {
                items.email = input.email;
                items.institution_name = input.institution_name;
                items.mobile = input.mobile;
                items.address = input.address;
                items.password = input.password;
                items.registration_doc_id = null;
                items.registration_doc_name = null;
                items.created_on = new Date();
                items.updated_on = null
                if(items.site_id === null || items.site_id === 'null'){
                    let customSiteId = await db.one("SELECT NEXTVAL('BDA_SITE_ID') AS new_site_id");
                    console.log('[==== SITE ID FOR CUSTOM SITE ====]',customSiteId)
                    items.site_id =  customSiteId.new_site_id;
                }
                return items
            })
            const finalOrganizationListArr = await Promise.all(mappedSiteDetail)
            console.log('mapped site details ', finalOrganizationListArr)
            
            let mappedOrganizations = await db.tx(t => {

                const q1 =  t.none('DELETE FROM BDA_TEMP_REGISTRATION_DTLS WHERE EMAIL = $1',[finalOrganizationListArr[0].email])

                const q2 = finalOrganizationListArr.map(l => {
    
                 t.none('INSERT INTO BDA_TEMP_REGISTRATION_DTLS(INSTITUTION_NAME,EMAIL,MOBILE,ADDRESS,PASSWORD,ALLOTTEE_ORGANISATION_NAME,LAYOUT_NAME,CA_SITE_NO,DIM_TOTAL,PURPOSE_NAME,SITE_FLAG,REGISTRATION_DOC_ID,REGISTRATION_DOC_NAME,CREATED_ON,UPLOADED_ON,SITE_ID) VALUES(${institution_name},${email},${mobile},${address},${password},${allottee_organisation_name},${layout_name},${ca_site_no},${dim_total},${purpose_name},${site_flag},${registration_doc_id},${registration_doc_name},${created_on},${updated_on},${site_id})',l)
                })
                return t.batch([q1,q2]);
            })

            if(!mappedOrganizations.length) {
                return {
                    status:false,
                    message:'failed'
                }
            }else{
                return {
                    status:true,
                    message:'success'
                }
            }
        } catch (error) {
            console.log(error)
            return {
                status:false,
                message:'InternalServerError'
            }
        }
    },

    institutionRegistration:async (input) => {
        console.log(input)
        try {
            let institutionProfile = input.registrationDetails;
            let mappedProfileDetails = await institutionProfile.map(async (profile,index) => {
                profile.created_on = new Date()
                profile.updated_on = null
                profile.updated_by = null
                profile.status = 'N',
                profile.h = `${profile.created_on.getHours()}`.padStart(2, '0')
                profile.m = `${profile.created_on.getMinutes()}`.padStart(2, '0')
                profile.s = `${profile.created_on.getSeconds()}`.padStart(2, '0')
                profile.registration_no = `${profile.created_on.getFullYear()}${profile.created_on.getMonth() + 1}${profile.created_on.getDate()}${profile.h}${profile.m}${profile.s + index}`
                profile.user_id = Date.now()
                return profile
            })
            const finalOrganizationListArr = await Promise.all(mappedProfileDetails)
            console.log('mapped site details ', finalOrganizationListArr)

            let checkUser = await db.one('SELECT COUNT(*) AS USER_CNT FROM BDA_USER_PROFILE WHERE EMAIL = $1',[finalOrganizationListArr[0].email.toLowerCase()])

            console.log('[=====User Email count===]',checkUser)
            if(checkUser.user_cnt > 0) return {
                status: false,
                message: 'User already exists'
            }

            let userPassword = await passwordHandler.encodePassword(finalOrganizationListArr[0].password);
            if (!userPassword) return {
                status: false,
                message: 'Please try again!!'
            }

            const query = await db.tx(t => {

                const q1 =  t.none('INSERT INTO BDA_USER_PROFILE(USER_ID,COMPANY_NAME,CONTACT_NO,EMAIL,USER_PASSWORD,COMPANY_ADDRESS,STATUS,USER_TYPE,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY)VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)',[finalOrganizationListArr[0].user_id,finalOrganizationListArr[0].institution_name,finalOrganizationListArr[0].mobile,finalOrganizationListArr[0].email,userPassword,finalOrganizationListArr[0].address,'Y','INSTITUTION',finalOrganizationListArr[0].created_on,finalOrganizationListArr[0].user_id,finalOrganizationListArr[0].updated_on,finalOrganizationListArr[0].updated_by])

                const q2 = finalOrganizationListArr.map(l => {
                    t.none ('INSERT INTO BDA_USER_ORG_MAPPING (USER_ID,SITE_ID,SITE_NAME,CA_SITE_NO,SITE_LAYOUT_NAME,SITE_DIMENSION,PURPOSE,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,SITE_FLAG,REGISTRATION_DOC,ORIGINAL_REGISTRATION_DOC,REGISTRATION_NO) VALUES(${user_id},${site_id},${allottee_organisation_name},${ca_site_no},${layout_name},${dim_total},${purpose_name},${status},${created_on},${user_id},${updated_on},${updated_by},${site_flag},${registration_doc_id},${registration_doc_name},${registration_no})', l);
                });

                const q3 = t.none('INSERT INTO BDA_FCM_NOTIFICATION(USER_ID,TOKEN,STATUS,CREATED_ON) VALUES($1,$2,$3,$4)', [finalOrganizationListArr[0].user_id, finalOrganizationListArr[0].token, 'Y', finalOrganizationListArr[0].created_on]);


                return t.batch([q1,q2,q3]);

            })

            

            let notificationData = {
                title: 'Thank you for getting in touch!',
                token:input.token,
                email: finalOrganizationListArr[0].email,
                template_id:21089981,
                form_id:finalOrganizationListArr,
                company_name:finalOrganizationListArr[0].institution_name
            }

            let token = await Notification.sendNotification(notificationData)
            if(!token) return {
                status: false,
                message: 'Failed to send notification'
            }

            if (query.length != 3) return {
                status: false,
                message: 'Failed to register'
            }

            let email = await SendEmail.sendMail(notificationData)
            if(!email) return {
                status: false,
                message: 'Failed to send email'
            }

            return {
                status: true,
                message: 'User registered successfully!!'
            }

        } catch (error) {
            console.log(error)
        }
    },

    checkUserExist: async (input) => {
        console.log('----------------------',input)
        try {
            let checkUser = await db.one('SELECT COUNT(*) AS USER_CNT FROM BDA_USER_PROFILE WHERE EMAIL = $1',[input.email.toLowerCase()])
            if(checkUser.user_cnt > 0){
                return {
                    status: false,
                    message: 'EmailAlreadyExist'
                }
            }else{
                return {
                    status: true,
                    message: 'NewUser'
                }
            }
        } catch (error) {
            return {
                status: false,
                message:'InternalServerError'
            }
        }
        

    }

}