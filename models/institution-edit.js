"use strict"

const db = require('../config/db').db;
const passwordHandler = require('../utils/password-handler')

module.exports = {

    //Change password for Institution
    updateInstitutionChangePassword: async (input) => {
        console.log('[==== Web/Mobile Institution Change Password Request ====]', input);
        try {
            let oldPassword = await db.one('SELECT USER_PASSWORD FROM BDA_USER_PROFILE WHERE USER_ID = $1 AND STATUS = $2', [input.user_id, 'Y'])
            if (!oldPassword) return {
                status: false,
                message: 'UserDoesNotExitErr!!'
            }

            let isValidPassword = await passwordHandler.comparePassword(input.user_password, oldPassword.user_password)
            if (!isValidPassword) return {
                status: false,
                message: 'InvalidOldPasswordErr!!'
            }

            if (input.new_password != input.confirm_password) return {
                status: false,
                message: 'NewPasswordAndConfirmPasswordMismatchErr!!'
            }

            let newPassword = await passwordHandler.encodePassword(input.confirm_password)
            if (!newPassword) return {
                status: false,
                message: 'PleaseTryAgainErr!!'
            }

            let insertNewPassword = await db.one('UPDATE BDA_USER_PROFILE SET USER_PASSWORD = $1, UPDATED_ON = $2, UPDATED_BY = $3 WHERE USER_ID = $4 RETURNING USER_ID', [newPassword, new Date(), input.user_id, input.user_id])
            if (!insertNewPassword) return {
                status: false,
                message: 'FailedToChangePasswordErr!!'
            }
            return {
                status: true,
                data: insertNewPassword,
                message: 'PasswordChanged!!'
            }
        } catch (error) {
            console.log('[==== Web/Mobile Institution Change Password Error ====]', error);
            return {
                status: false,
                message: 'InternalError'
            }
        }
    },

    //Update profile for institution
    updateInstitutionProfile: async (input) => {
        console.log('[==== Web/Mobile Institution Update Profile Request ====]', input);
        try {
            input.email = input.email.toLowerCase();
            let userProfile = await db.one('UPDATE BDA_USER_PROFILE SET EMAIL = $1, CONTACT_NO = $2 WHERE USER_ID = $3 RETURNING USER_ID', [input.email,input.contact_no,input.user_id])
            if (!userProfile) return {
                status: false,
                message: 'FailedToUpdateProfileErr!!'
            }
            return {
                status: true,
                data: userProfile,
                message: 'ProfileUpdated!!'
            }
        } catch (error) {
            console.log('[==== Web/Mobile Institution Update Profile Error ====]', error);
            return {
                status: false,
                message: 'InternalError'
            }
        }
    },

    //View profile
    viewInstitutionProfile:async(input) => {
        console.log('[==== Web/Mobile Institution Profile Request ====]', input);
        try {
            let profile = await db.one('SELECT USER_ID,COMPANY_NAME,CONTACT_NO,EMAIL,COMPANY_ADDRESS,REGISTRATION_DOC,STATUS,USER_TYPE,CREATED_ON FROM BDA_USER_PROFILE WHERE USER_ID = $1',[input.user_id])
            if(!profile) return {
                status: false,
                message: 'PleaseTryAgain'
            }
            return {
                status: true,
                data: profile,
                message:'ProfileFetched'
            }
        } catch (error) {
            console.log('[==== Web/Mobile Institution Profile Error ====]', error);
            return {
                status: false,
                message: 'InternalError'
            }
        }
    }
}