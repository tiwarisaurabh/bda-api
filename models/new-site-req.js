'use strict'

const db = require('../config/db').db;
const uploadFolder = require('../config/constant');
const docHelper = require('../utils/image-handler');
const fs = require('fs')


module.exports = {

    //For adding new site request
    addNewSite:async(input) => {
        console.log('[========== addNewSite Req ==========]',input)
        try {
            let selectedSite = input.site;
            let flagMapping = await selectedSite.map(async (item) => {
                if(item.site_id === null || item.site_id === 'null'){
                    let customSiteId = await db.one("SELECT NEXTVAL('BDA_SITE_ID') AS new_site_id");
                    console.log('[==== SITE ID FOR CUSTOM SITE ====]',customSiteId)
                    item.site_id =  customSiteId.new_site_id;
                }
                console.log('item',item)
                return item;
            })

            //console.log('flagMapping',flagMapping)

            const finalOrganizationListArr = await Promise.all(flagMapping)
            // console.log('finalOrganizationListArr',finalOrganizationListArr)
            let date = new Date()
            finalOrganizationListArr.map((item) => {
                item.status = 'N',
                item.created_by = input.user_id,
                item.updated_by = null, 
                item.created_on = date, 
                item.updated_on = null,
                item.user_id = input.user_id
            })
            //console.log('finalOrganizationListArr',finalOrganizationListArr)
            let mappedOrganizations = await db.tx(t => {
                const query = finalOrganizationListArr.map(l => {
                    t.none('INSERT INTO BDA_NEW_SITE_REQUEST(USER_ID,SITE_ID,SITE_NAME,CA_SITE_NO,SITE_LAYOUT_NAME,SITE_DIMENSION,PURPOSE,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,SITE_FLAG) VALUES(${user_id},${site_id},${allottee_organisation_name},${ca_site_no},${layout_name},${dim_total},${purpose_name},${status},${created_on},${created_by},${updated_on},${updated_by},${site_flag})',l)
                })
                return t.batch(query);
            })

            if(!mappedOrganizations.length) {
                return {
                    status:false,
                    message:'NoRecordFound'
                }
            }else{
                return {
                    status:true,
                    message:'Success'
                }
            }
            
            
        } catch (error) {
            console.log('[========== addNewSite Err ==========]',error)
            return {
                status:false,
                message:'InternalServerError'
            }
        }
    },

    //Approve new site request
    approveNewSiteReq: async(input) => {
        console.log('[========== approveNewSiteReq Req ==========]',input)
        input.updated_on = new Date()
        try {
            let approveSite = await db.tx(t => {

                const q1 = t.one('UPDATE BDA_NEW_SITE_REQUEST SET STATUS=$1,UPDATED_ON=$2,UPDATED_BY=$3 WHERE USER_ID=$4 AND SITE_ID=$5 RETURNING USER_ID',['Y',input.updated_on,input.created_by,input.user_id,input.alloted_organization[0].site_id])

                const q2 = t.one('INSERT INTO BDA_USER_ORG_MAPPING(USER_ID,SITE_ID,SITE_NAME,CA_SITE_NO,SITE_LAYOUT_NAME,SITE_DIMENSION,PURPOSE,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,SITE_FLAG) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13) RETURNING USER_ID',[input.user_id,input.alloted_organization[0].site_id,input.alloted_organization[0].allottee_organisation_name,input.alloted_organization[0].ca_site_no,input.alloted_organization[0].layout_name,input.alloted_organization[0].dim_total,input.alloted_organization[0].purpose_name,input.alloted_organization[0].status,input.updated_on,input.created_by,input.updated_on,input.created_by,input.site_flag])

                return t.batch([q1,q2])

            })

            //console.log('====================',approveSite.length)
            if(!approveSite.length) {
                return {
                    status:false,
                    message:'NoRecordFound'
                }
            }else {
                return {
                    status:true,
                    message:'Success'
                }
            }
        } catch (error) {
            console.log('[========== approveNewSiteReq Err ==========]',error)
            return {
                status:false,
                message:'InternalServerError'
            }
        }
    },

    getPendingNewSite:async() => {
        console.log('[========== getPendingNewSite ==========]')
        try {
            let pendingNewSite = await db.any('SELECT A.USER_ID, A.COMPANY_NAME, A.CONTACT_NO, A.EMAIL, A.COMPANY_ADDRESS, A.REGISTRATION_DOC,A.ORIGINAL_FILE AS ORIGINAL_REGISTRATION_DOC, B.SITE_ID, B.SITE_NAME AS ALLOTTEE_ORGANISATION_NAME, B.CA_SITE_NO , B.SITE_LAYOUT_NAME AS LAYOUT_NAME, B.SITE_DIMENSION AS DIM_TOTAL, B.PURPOSE AS PURPOSE_NAME,B.CREATED_ON,B.STATUS,B.SITE_FLAG,B.UPDATED_ON AS APPROVED_DATE FROM BDA_USER_PROFILE A INNER JOIN BDA_NEW_SITE_REQUEST B ON A.USER_ID = B.USER_ID ORDER BY CREATED_ON DESC')

            if(!pendingNewSite.length){
                return {
                    status:false,
                    message:'NoRecordFound'
                }
            }else {
                return {
                    status:true,
                    data:pendingNewSite,
                    message:'Success'
                }
            }
        } catch (error) {
            console.log('[========== getPendingNewSite ==========]',error)
            return {
                status:false,
                message:'InternalServerError'
            }
        }
    },

    rejectPendingNewSite:async(input) => {
        console.log('[========== rejectPendingNewSite Req ==========]',input)
        input.updated_on = new Date()
        try {
            let rejectSite = await db.one('UPDATE BDA_NEW_SITE_REQUEST SET STATUS = $1,UPDATED_ON = $2,UPDATED_BY = $3 WHERE USER_ID = $4 AND SITE_ID=$5 RETURNING USER_ID',['R',input.updated_on,input.updated_by,input.user_id,input.site_id])

            if(!rejectSite) {
                return {
                    status:false,
                    message:'NoRecordFound'
                }
            }else {
                return {
                    status:true,
                    message:'Success'
                }
            }
        } catch (error) {
            console.log('[========== rejectPendingNewSite Err ==========]',error)
            return {
                status:false,
                message:'InternalServerError'
            }
        }
    },

    storeNewSiteRequestAsTemp:async(input) => {
        console.log('============= storeNewSiteRequestAsTemp Request ============',input)
        try {
            let siteDetails = input.site_name;
            let mappedSiteDetail = await siteDetails.map(async (items) => {
                items.registration_doc_id = null;
                items.registration_doc_name = null;
                items.user_id = input.user_id;
                items.updated_on = null
                items.created_on = new Date();
                if(items.site_id === null || items.site_id === 'null'){
                    let customSiteId = await db.one("SELECT NEXTVAL('BDA_SITE_ID') AS new_site_id");
                    console.log('[==== SITE ID FOR CUSTOM SITE ====]',customSiteId)
                    items.site_id =  customSiteId.new_site_id;
                }
                return items
            })
            const finalOrganizationListArr = await Promise.all(mappedSiteDetail)
            console.log('mapped site details ', finalOrganizationListArr)
            
            let mappedOrganizations = await db.tx(t => {

                const q1 =  t.none('DELETE FROM BDA_TEMP_NEW_SITE_REQ WHERE USER_ID = $1',[finalOrganizationListArr[0].user_id])

                const q2 = finalOrganizationListArr.map(l => {
    
                 t.none('INSERT INTO BDA_TEMP_NEW_SITE_REQ(USER_ID,SITE_ID,ALLOTTEE_ORGANISATION_NAME,LAYOUT_NAME,CA_SITE_NO,DIM_TOTAL,PURPOSE_NAME,SITE_FLAG,REGISTRATION_DOC_ID,REGISTRATION_DOC_NAME,CREATED_ON,UPLOADED_ON) VALUES(${user_id},${site_id},${allottee_organisation_name},${layout_name},${ca_site_no},${dim_total},${purpose_name},${site_flag},${registration_doc_id},${registration_doc_name},${created_on},${updated_on})',l)
                })
                return t.batch([q1,q2]);
            })

            if(!mappedOrganizations.length) {
                return {
                    status:false,
                    message:'failed'
                }
            }else{
                return {
                    status:true,
                    message:'success'
                }
            }
        } catch (error) {
            console.log(error)
            return {
                status:false,
                message:'InternalServerError'
            }
        }
    },

    getTempNewSiteReq:async(input) => {
        try {
            let uploadedFile = await db.any('SELECT * FROM BDA_TEMP_NEW_SITE_REQ WHERE USER_ID = $1 ORDER BY SITE_ID DESC', [input.user_id])
            if (!uploadedFile) {
                return {
                    status: false,
                    message: 'NoRecordFound'
                }
            } else {
                return {
                    status: true,
                    data: uploadedFile,
                    message: 'Success'
                }
            }
        } catch (error) {
            console.log('getRegistrationUploadedFile', error)
            return {
                status: false,
                message: 'InternalServerError'
            }
        }
    },

    uploadRegistrationCertificate: async (input) => {
        console.log('[====== Upload Registration Certificate  =======]', input)

        //For image handling
        if (input.file_type === 'pdf') {
            var decodedPdf = await docHelper.decodeBase64Pdf(input.upload_img);
            var imageBuffer = await decodedPdf.data;
            var type = await decodedPdf.type;
            var fileName = Date.now() + '.' + type;

        } else {
            var decodedImg = await docHelper.decodeBase64Image(input.upload_img);
            var imageBuffer = await decodedImg.data;
            var type = await decodedImg.type;
            var fileName = Date.now() + '.' + type;
        }

        //For uploading image
        try {
            fs.writeFileSync(uploadFolder.identityPath + fileName, imageBuffer, 'base64');
        } catch (err) {
            console.error(err)
            return {
                status: false,
                message: 'failed'
            }
        }
        try {
            let order = await db.one('UPDATE BDA_TEMP_NEW_SITE_REQ SET REGISTRATION_DOC_ID = $1, REGISTRATION_DOC_NAME = $2, UPLOADED_ON = $3 WHERE USER_ID = $4 AND SITE_ID = $5 RETURNING SITE_ID',[fileName,input.registration_file_name,new Date(),input.user_id,input.site_id])

            if(!order){
                return {
                    status: false,
                    message:'failed'
                }
            }else{
                return {
                    status: true,
                    data: order,
                    message: 'success'
                }
            }
            
        } catch (error) {
            console.log('[====== Upload Registration Certificate Error =======]', error)
            return {
                status: false,
                message: 'InternalError'
            }
        }
    },

    institutionRegistration:async (input) => {
        console.log(input)
        try {
            let institutionProfile = input.registrationDetails;
            let mappedProfileDetails = await institutionProfile.map(async (profile,index) => {
                profile.created_on = new Date()
                profile.updated_on = null
                profile.updated_by = null
                profile.status = 'N',
                profile.h = `${profile.created_on.getHours()}`.padStart(2, '0')
                profile.m = `${profile.created_on.getMinutes()}`.padStart(2, '0')
                profile.s = `${profile.created_on.getSeconds()}`.padStart(2, '0')
                profile.registration_no = `${profile.created_on.getFullYear()}${profile.created_on.getMonth() + 1}${profile.created_on.getDate()}${profile.h}${profile.m}${profile.s + index}`
                profile.user_id = input.user_id
                return profile
            })

            const finalOrganizationListArr = await Promise.all(mappedProfileDetails)
            console.log('mapped site details ', finalOrganizationListArr)
            
            const query = await db.tx(t => {
                const q1 = finalOrganizationListArr.map(l => {
                    t.none ('INSERT INTO BDA_USER_ORG_MAPPING (USER_ID,SITE_ID,SITE_NAME,CA_SITE_NO,SITE_LAYOUT_NAME,SITE_DIMENSION,PURPOSE,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,SITE_FLAG,REGISTRATION_DOC,ORIGINAL_REGISTRATION_DOC,REGISTRATION_NO) VALUES(${user_id},${site_id},${allottee_organisation_name},${ca_site_no},${layout_name},${dim_total},${purpose_name},${status},${created_on},${user_id},${updated_on},${updated_by},${site_flag},${registration_doc_id},${registration_doc_name},${registration_no})', l);
                });

                return t.batch(q1);

            })

            if (!query.length) return {
                status: false,
                message: 'failed'
            }

            return {
                status: true,
                message: 'success'
            }

        } catch (error) {
            console.log(error)
            return {
                status: true,
                message: 'InternalServerError'
            }
        }
    },

    deleteUploadedCertificate:async (input) => {
        console.log('Delete registration certificate',input)
        try {
            let document = await db.one('UPDATE BDA_TEMP_NEW_SITE_REQ SET REGISTRATION_DOC_ID = $1, REGISTRATION_DOC_NAME = $2 WHERE USER_ID = $3 AND SITE_ID = $4 RETURNING SITE_ID',[null,null,input.user_id,input.site_id])
            if(!document) {
                return {
                    status: false,
                    message: 'failed'
                }
            }else{
                return {
                    status: true,
                    message: 'success'
                }
            }
        } catch (error) {
            console.log('delete document error', error)
            return {
                status: false,
                message: 'InternalServerError'
            }
        }
    },
}




