'use strict';

const db = require('../config/db').db;
const TriggerMail = require('../utils/send-email');
const msg91Conf = require('../config/constant').msg91;
const msg91 = require("msg91")(msg91Conf.MSG91_AUTH_KEY, msg91Conf.MSG91_SENDER_ID, msg91Conf.ROUTE_NO);
const Notification = require("../utils/fcm-notification");

module.exports = {

    //Service to get the pending user details for web-portal
    getPendingUsers: async () => {
        try {
            let status = ['N', 'R']
            let user = await db.any('select A.COMPANY_NAME, A.CONTACT_NO, A.EMAIL, A.COMPANY_ADDRESS, B.REGISTRATION_NO as FORM_ID,B.USER_ID, B.SITE_ID, B.SITE_NAME, B.CA_SITE_NO, B.SITE_LAYOUT_NAME, B.SITE_DIMENSION, B.PURPOSE, B.STATUS, B.CREATED_ON as REGISTRATION_DATE, B.UPDATED_ON as APPROVED_DATE, B.SITE_FLAG, B.REGISTRATION_DOC, B.ORIGINAL_REGISTRATION_DOC, B.REMARK, C.FNAME AS APPROVER_FNAME, C.LNAME AS APPROVER_LNAME from bda_user_profile A inner join bda_user_org_mapping B on A.user_id = B.user_id left outer join ( select user_id, fname, lname from bda_employee_profile) C on b.updated_by = c.user_id where B.STATUS in ($1:csv) AND B.SITE_FLAG = $2 ORDER BY B.CREATED_ON DESC', [status,'M'])
            if (!user) return {
                status: false,
                message: 'No records found'
            }
            console.log('===========', user)
            return {
                status: true,
                data: user,
                message: 'Pending user list fetched'
            }
        } catch (error) {
            console.log(error);
            return {
                status: false,
                message: 'Please try again!!'
            }
        }

    },

    approveInstitutionRegistration: async (input) => {
        console.log('[======== Approve institution registration Req ========]', input)
        input.updated_on = new Date()

        try {

            let approveRegistration = await db.tx(t => {

                const q1 = t.one('UPDATE BDA_USER_ORG_MAPPING SET STATUS = $1, UPDATED_ON = $2, UPDATED_BY = $3, REMARK = $4 WHERE REGISTRATION_NO = $5 AND USER_ID = $6 RETURNING REGISTRATION_NO', ['Y', input.updated_on, input.approved_by,input.remark,input.registration_no, input.institution_uid])

                const q2 = t.one('SELECT TOKEN FROM BDA_FCM_NOTIFICATION WHERE USER_ID = $1 ORDER BY CREATED_ON DESC LIMIT 1', [input.institution_uid])

                //const q3 = t.func('BDA_INSERT_CUSTOM_SITE_REQ_FUNC',[input.institution_uid,input.registration_no])

                return t.batch([q1, q2])
            })

            if (!approveRegistration.length) {
                return {
                    status: false,
                    message: 'failed'
                }
            } else {

                let notificationData = {
                    title: 'Registration Approved',
                    email: input.email,
                    template_id: 19851018,
                    mobileNumber: input.contact_no,
                    company_name: input.company_name,
                    form_id: input.registration_no,
                    token: approveRegistration[1].token,
                    body: `Hello ${input.company_name},\nCongratulations!\nYour Application Request for Registration ID: #${input.registration_no} have been approved successfully. Kindly login to check your account details.`
                }

                let token = await Notification.sendNotification(notificationData)
                if (!token) return {
                    status: false,
                    message: 'Failed to send notification'
                }

                let email = await TriggerMail.sendMail(notificationData)
                if (!email) return {
                    status: false,
                    message: 'Failed to send email'
                }

                let smsText = msg91.send(notificationData.mobileNumber, notificationData.body, function (err, res) {
                    if (res) {
                        console.log('[=== sms text response ===]', res)
                        return res
                    } else {
                        return err
                    }
                })

                return {
                    status: true,
                    data: approveRegistration,
                    message: 'Approved'
                }
            }
        } catch (error) {
            console.log('Approve Institution Registration Error',error)
            return {
                status: false,
                message: 'InternalServerError'
            }
        }
    },

   
    rejectInstitutionRegistration: async (input) => {
        console.log('========= Reject institution registration Request ==========', input)
        input.updated_on = new Date()
        try {

            let rejectRegistration = await db.tx(t => {

               
                const q1 = t.one('UPDATE BDA_USER_ORG_MAPPING SET STATUS = $1, UPDATED_ON = $2, UPDATED_BY = $3, REMARK = $4 WHERE REGISTRATION_NO = $5 AND USER_ID = $6 RETURNING REGISTRATION_NO', ['R', input.updated_on, input.approved_by, input.remark, input.registration_no, input.institution_uid])

                const q2 = t.one('SELECT TOKEN FROM BDA_FCM_NOTIFICATION WHERE USER_ID = $1 ORDER BY CREATED_ON DESC LIMIT 1', [input.institution_uid])

                //const q3 = t.func('BDA_INSERT_CUSTOM_SITE_REQ_FUNC',[input.institution_uid,input.registration_no])

                return t.batch([q1, q2])
            })

            if (!rejectRegistration.length) {
                return {
                    status: false,
                    message: 'failed'
                }
            } else {

                let notificationData = {
                    title: 'Registration Rejected',
                    email: input.email,
                    template_id: 20111374,
                    mobileNumber: input.contact_no,
                    company_name: input.company_name,
                    form_id: input.registration_no,
                    token: rejectRegistration[1].token,
                    body: `Hello ${input.company_name},\nWe are sorry to inform you that Your Application Request for Registration ID: #${input.registration_no} was rejected.`
                }

                let token = await Notification.sendNotification(notificationData)
                if (!token) return {
                    status: false,
                    message: 'Failed to send notification'
                }

                let email = await TriggerMail.sendMail(notificationData)
                if (!email) return {
                    status: false,
                    message: 'Failed to send email'
                }

                let smsText = msg91.send(notificationData.mobileNumber, notificationData.body, function (err, res) {
                    if (res) {
                        console.log('[=== sms text response ===]', res)
                        return res
                    } else {
                        return err
                    }
                })

                return {
                    status: true,
                    data: approveRegistration,
                    message: 'Rejected'
                }
            }
        } catch (error) {
            console.log('Reject Institution Registration Error', error)
            return {
                status: false,
                message: 'InternalServerError'
            }
        }

    },

    //Service to get the list of approved user details for web-portal
    getApprovedUser: async () => {
        try {

            let user = await db.any('SELECT A.COMPANY_NAME, A.CONTACT_NO, A.EMAIL, A.COMPANY_ADDRESS, B.REGISTRATION_NO as FORM_ID,B.USER_ID, B.SITE_ID, B.SITE_NAME, B.CA_SITE_NO, B.SITE_LAYOUT_NAME, B.SITE_DIMENSION, B.PURPOSE, B.STATUS, B.CREATED_ON as REGISTRATION_DATE, B.UPDATED_ON as APPROVED_DATE, B.SITE_FLAG, B.REGISTRATION_DOC, B.ORIGINAL_REGISTRATION_DOC, B.REMARK, C.FNAME AS APPROVER_FNAME, C.LNAME AS APPROVER_LNAME from bda_user_profile A inner join bda_user_org_mapping B on A.user_id = B.user_id left outer join ( select user_id, fname, lname from bda_employee_profile) C on b.updated_by = c.user_id where B.STATUS = $1 AND B.SITE_FLAG = $2 ORDER BY B.UPDATED_ON DESC',['Y','M'])
            if (!user) return {
                status: false,
                message: 'No Record Found'
            }
            return {
                status: true,
                data: user,
                message: 'Fetched Approved User'
            }
        } catch (error) {
            console.log('[======== Approve User List For Web Error ========]', error);
            return {
                status: false,
                message: 'Please try again!!'
            }
        }
    },

    //To check registration status
    checkRegistrationStatus: async (input) => {
        console.log('[======== Registration status check For Web Request ========]', input)
        try {
            let status = await db.one('SELECT STATUS FROM BDA_USER_ORG_MAPPING WHERE REGISTRATION_NO = $1', [input.registration_no])
            if (!status) {
                return {
                    status: false,
                    message: 'NoRecordFound'
                }
            }
            return {
                status: true,
                data: status,
                message: 'RecordsFetched'
            }
        } catch (error) {
            console.log('[======== Registration status check For Web Error ========]', error)
            return {
                status: false,
                message: 'InternalError'
            }
        }
    }
}