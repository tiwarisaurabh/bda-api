'use strict';

const db = require('../config/db').db;
const fs = require('fs');
const docHelper = require('../utils/image-handler');
const uploadFolder = require('../config/constant')

module.exports = {

    //To insert the new purchase order for the first time mobile app (step = 1)
    insertPurchaseOrder: async (input) => {
        console.log('[======== Insert New Purchase Order Mobile App Request ========]',input.original_purchase_copy,input.original_registration_copy,)
        input.status = 'P';
        input.created_on = new Date();
        input.order_id = Date.now();
        if(input.original_purchase_copy.split('.').pop() === 'pdf'){
            var decodedPdf = await docHelper.decodeBase64Pdf(input.purchase_copy);
            var imageBuffer = await decodedPdf.data;
            var type = await decodedPdf.type
            var fileName = Date.now() + '.' + type;
        }else{
            var decodedImg = await docHelper.decodeBase64Image(input.purchase_copy);
            var imageBuffer = await decodedImg.data;
            var type = await decodedImg.type
            var fileName = Date.now() + '.' + type;
        }

        if(input.original_registration_copy.split('.').pop() === 'pdf'){
            var decodedRegPdf = await docHelper.decodeBase64Pdf(input.registration_copy);
            var reg_imageBuffer = await decodedRegPdf.data;
            var type_reg = await decodedRegPdf.type
            var reg_fileName = Date.now() + '.' + type_reg;
        }else{
            var decodedRegImg = await docHelper.decodeBase64Image(input.registration_copy);
            var reg_imageBuffer = await decodedRegImg.data;
            var type_reg = await decodedRegImg.type
            var reg_fileName = Date.now() + '.' + type_reg;
        }

        console.log('---------------',reg_imageBuffer)
        try {
            fs.writeFileSync(uploadFolder.identityPath + fileName, imageBuffer, 'base64');
            fs.writeFileSync(uploadFolder.identityPath + reg_fileName, reg_imageBuffer, 'base64');

        } catch (err) {
            console.error(err)
            return {
                status: false,
                message: 'Failed to upload file'
            }
        }

        try {

            let purchaseOrderId = await db.one("SELECT nextval('bda_purchase_id') AS purchase_id")
            if (!purchaseOrderId) return {
                status: false,
                message: 'PurchaseIDSeqErr'
            }

            let request_id = await db.one("SELECT nextval('request_request_id_seq') AS request_id")
            if (!purchaseOrderId) return {
                status: false,
                message: 'REQUESTIDERR'
            }
            console.log(request_id);

            var date = new Date();

            input.purchase_id = `BDA/CA/${purchaseOrderId.purchase_id}`

            let order = await db.tx(t => {

                //uncomment this query

                // const q1 = t.none('INSERT INTO BDA_PURCHASE_AUDIT_HISTORY(ORDER_ID,PURCHASE_ID,AUTHORIZED_PERSON,SITE_ID,LAYOUT_NAME,CA_SITE_NO,ALLOTTEE_ORGANISATION_NAME,DIM_EW,DIM_NS,DIM_TOTAL,DIM_TYPE,EAST,WEST,NORTH,SOUTH,PURPOSE_NAME,DATE_OF_LEASE_AGREEMENT,AMOUNT,LEASE_REGN_NO,BOARD_RES_NUMBER,BOARD_RES_DATE,RESOLUTION_NO,RESOLUTION_DATE,LEASE_EXP_DATE,PRESENT_INSTUTION_DTL,ACTION_DTL_CA_SITE_ALLOTTED,PURPOSE_DTL,APPROVED_PLAN_DTL,APPROVED_PLAN_SMRY,CONSTRUCTION_VARIATION,POSTAL_ADDRESS,OTHER_DTL,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY)VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26, $27, $28,$29, $30, $31, $32, $33, $34, $35, $36, $37)', [input.order_id, input.purchase_id, input.authorized_person, input.site_id, input.layout_name, input.ca_site_no, input.allottee_organisation_name, input.dim_ew, input.dim_ns, input.dim_total, input.dim_type, input.east, input.west, input.north, input.south, input.purpose_name, input.date_of_lease_agreement, input.amount, input.lease_regn_no, input.board_res_number, input.board_res_date, input.resolution_no, input.resolution_date, input.lease_exp_date, input.present_instution_dtl, input.action_dtl_ca_site_allotted, input.purpose_dtl, input.approved_plan_dtl, input.approved_plan_smry, input.construction_variation, input.postal_address, input.other_dtl, input.status, input.created_on, input.user_id, null, null]);
                

                const q2 = t.none('INSERT INTO BDA_PURCHASE_ORDER(ORDER_ID,PURCHASE_ID,AUTHORIZED_PERSON,SITE_ID,LAYOUT_NAME,CA_SITE_NO,ALLOTTEE_ORGANISATION_NAME,DIM_EW,DIM_NS,DIM_TOTAL,DIM_TYPE,EAST,WEST,NORTH,SOUTH,PURPOSE_NAME,DATE_OF_LEASE_AGREEMENT,AMOUNT,LEASE_REGN_NO,BOARD_RES_NUMBER,BOARD_RES_DATE,RESOLUTION_NO,RESOLUTION_DATE,LEASE_EXP_DATE,PRESENT_INSTUTION_DTL,ACTION_DTL_CA_SITE_ALLOTTED,PURPOSE_DTL,APPROVED_PLAN_DTL,APPROVED_PLAN_SMRY,CONSTRUCTION_VARIATION,POSTAL_ADDRESS,OTHER_DTL,STATUS,CREATED_ON,CREATED_BY,REGISTRATION_DOC, PURCHASE_DOC,ORIGINAL_REGISTRATION_DOC,ORIGINAL_PURCHASE_DOC) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26, $27, $28,$29, $30, $31, $32, $33, $34, $35, $36, $37,$38,$39)', [input.order_id, input.purchase_id, input.authorized_person, input.site_id, input.layout_name, input.ca_site_no, input.allottee_organisation_name, input.dim_ew, input.dim_ns, input.dim_total, input.dim_type, input.east, input.west, input.north, input.south, input.purpose_name, input.lease_regn_date, input.amount, input.lease_regn_no, input.board_res_number, input.board_res_date, input.resolution_no, input.resolution_date, input.lease_exp_date, input.present_instution_dtl, input.action_dtl_ca_site_allotted, input.purpose_dtl, input.approved_plan_dtl, input.approved_plan_smry, input.construction_variation, input.postal_address, input.other_dtl, input.status, input.created_on, input.user_id, reg_fileName, fileName,input.original_registration_copy,input.original_purchase_copy])

                const q3 = t.none('INSERT INTO REQUEST(REQUEST_ID,TITLE,REQUEST_TYPE_ID,USER_ID,CREATED_ON) VALUES($1,$2,$3,$4,$5)',[request_id.request_id,'PURCHASE ORDER','1',input.user_id,date])

                const q4 = t.none('INSERT INTO REQUEST_DATA(REQUEST_DATA_ID,REQUEST_ID) VALUES($1,$2)', [input.order_id,request_id.request_id])

                const q5 = t.none('INSERT INTO TRANSITION(CURRENT_STATE_ID,NEXT_STATE_ID,REQUEST_ID,CREATED_ON) VALUES($1,$2,$3,$4)',['1','2',request_id.request_id,date])

                return t.batch([q2,q3,q4,q5])

            })

            if (!order.length) return {
                status: false,
                message: 'No Record Found'
            }

            return {
                status: true,
                data: {
                    status: input.status,
                    order_id: input.order_id,
                    purchase_id: input.purchase_id
                },
                message: "Order Placed Successfully"
            }

        } catch (error) {
            console.log('[========== Insert New Purchase Order Mobile App Error ==========]', error)
            return {
                status: false,
                message: 'Internal Server Error'
            }
        }

    },

    //To audit the purchase order request from web-portal (step = 2)
    auditPurchaseOrder: async (input) => {
        console.log('[======== Insert Purchase Audit Web-portal Request ========]', input)
        input.updated_on = new Date();
        // let purchaseOrderId = await db.one("SELECT nextval('bda_purchase_id') AS purchase_id")
        // if (!purchaseOrderId) return {
        //     status: false,
        //     message: 'PurchaseIDSeqErr'
        // }

        // input.purchase_id = `BDA/CA/${purchaseOrderId.purchase_id}`

        if (input.status === 'R') {
            try {

                let auditReq = await db.tx(t => {

                    const q1 = t.none('INSERT INTO BDA_PURCHASE_AUDIT_HISTORY(ORDER_ID,PURCHASE_ID,AUTHORIZED_PERSON,SITE_ID,LAYOUT_NAME,CA_SITE_NO,ALLOTTEE_ORGANISATION_NAME,DIM_EW,DIM_NS,DIM_TOTAL,DIM_TYPE,EAST,WEST,NORTH,SOUTH,PURPOSE_NAME,DATE_OF_LEASE_AGREEMENT,AMOUNT,LEASE_REGN_NO,BOARD_RES_NUMBER,BOARD_RES_DATE,RESOLUTION_NO,RESOLUTION_DATE,LEASE_EXP_DATE,PRESENT_INSTUTION_DTL,ACTION_DTL_CA_SITE_ALLOTTED,PURPOSE_DTL,APPROVED_PLAN_DTL,APPROVED_PLAN_SMRY,CONSTRUCTION_VARIATION,POSTAL_ADDRESS,OTHER_DTL,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,TAG_NAME,REGISTRATION_DOC,PURCHASE_DOC,VERIFIED_PURCHASE_DOC,ORIGINAL_REGISTRATION_DOC,ORIGINAL_PURCHASE_DOC,ORIGINAL_VERIFIED_PURCHASE_DOC,INSERTION_DATE) SELECT ORDER_ID,PURCHASE_ID,AUTHORIZED_PERSON,SITE_ID,LAYOUT_NAME,CA_SITE_NO,ALLOTTEE_ORGANISATION_NAME,DIM_EW,DIM_NS,DIM_TOTAL,DIM_TYPE,EAST,WEST,NORTH,SOUTH,PURPOSE_NAME,DATE_OF_LEASE_AGREEMENT,AMOUNT,LEASE_REGN_NO,BOARD_RES_NUMBER,BOARD_RES_DATE,RESOLUTION_NO,RESOLUTION_DATE,LEASE_EXP_DATE,PRESENT_INSTUTION_DTL,ACTION_DTL_CA_SITE_ALLOTTED,PURPOSE_DTL,APPROVED_PLAN_DTL,APPROVED_PLAN_SMRY,CONSTRUCTION_VARIATION,POSTAL_ADDRESS,OTHER_DTL,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,TAG_NAME,REGISTRATION_DOC,PURCHASE_DOC,VERIFIED_PURCHASE_DOC,ORIGINAL_REGISTRATION_DOC,ORIGINAL_PURCHASE_DOC,ORIGINAL_VERIFIED_PURCHASE_DOC,NOW() FROM BDA_PURCHASE_ORDER WHERE ORDER_ID = $1 AND PURCHASE_ID = $2',[input.order_id, input.purchase_id])

                    const q2 = t.none('UPDATE BDA_PURCHASE_ORDER SET  STATUS = $1, UPDATED_ON = $2, UPDATED_BY = $3,tag_name = $4 WHERE ORDER_ID = $5 AND PURCHASE_ID = $6', ['R', input.updated_on, input.user_id,JSON.stringify(input.tag_name), input.order_id, input.purchase_id])

                    

                    // const q2 = t.none('INSERT INTO BDA_PURCHASE_AUDIT_HISTORY(ORDER_ID,PURCHASE_ID,AUTHORIZED_PERSON,SITE_ID,LAYOUT_NAME,CA_SITE_NO,ALLOTTEE_ORGANISATION_NAME,DIM_EW,DIM_NS,DIM_TOTAL,DIM_TYPE,EAST,WEST,NORTH,SOUTH,PURPOSE_NAME,DATE_OF_LEASE_AGREEMENT,AMOUNT,LEASE_REGN_NO,BOARD_RES_NUMBER,BOARD_RES_DATE,RESOLUTION_NO,RESOLUTION_DATE,LEASE_EXP_DATE,PRESENT_INSTUTION_DTL,ACTION_DTL_CA_SITE_ALLOTTED,PURPOSE_DTL,APPROVED_PLAN_DTL,APPROVED_PLAN_SMRY,CONSTRUCTION_VARIATION,POSTAL_ADDRESS,OTHER_DTL,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY)VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26,$27, $28, $29, $30, $31, $32, $33, $34, $35, $36, $37)', [input.order_id, input.purchase_id, input.authorized_person, input.site_id, input.layout_name, input.ca_site_no, input.allottee_organisation_name, input.dim_ew, input.dim_ns, input.dim_total, input.dim_type, input.east, input.west, input.north, input.south, input.purpose_name, input.date_of_lease_agreement, input.amount, input.lease_regn_no, input.board_res_number, input.board_res_date, input.resolution_no, input.resolution_date, input.lease_exp_date, input.present_instution_dtl, input.action_dtl_ca_site_allotted, input.purpose_dtl, input.approved_plan_dtl, input.approved_plan_smry, input.construction_variation, input.postal_address, input.other_dtl, 'R', input.created_on, input.created_by, input.updated_on, input.user_id]);

                    // const q3 = t.none('INSERT INTO BDA_PURCHASE_AUDIT_TAG(ORDER_ID,PURCHASE_ID,TAG_NAME,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY) VALUES($1, $2,$3, $4, $5, $6, $7, $8)', [input.order_id, input.purchase_id, JSON.stringify(input.tag_name), 'R', input.created_on, input.created_by, input.updated_on, input.user_id]);

                    return t.batch([q1, q2])

                })

                if (!auditReq.length) return {
                    status: false,
                    message: 'DatabaseError'
                }

                return {
                    status: true,
                    data: {
                        order_id: input.order_id,
                        purchase_id: input.purchase_id,
                        status: input.status
                    },
                    message: 'PurchaseSubmitted'
                }
            } catch (error) {
                console.log('=======In Review Audit Error Purchase========', error)
                return {
                    status: false,
                    message: 'InternalError'
                }
            }
        } else {
            try {
                let finalisedPurchase = await db.tx(t => {

                    // const q1 = t.none('INSERT INTO BDA_PURCHASE_AUDIT_HISTORY(ORDER_ID,PURCHASE_ID,AUTHORIZED_PERSON,SITE_ID,LAYOUT_NAME,CA_SITE_NO,ALLOTTEE_ORGANISATION_NAME,DIM_EW,DIM_NS,DIM_TOTAL,DIM_TYPE,EAST,WEST,NORTH,SOUTH,PURPOSE_NAME,DATE_OF_LEASE_AGREEMENT,AMOUNT,LEASE_REGN_NO,BOARD_RES_NUMBER,BOARD_RES_DATE,RESOLUTION_NO,RESOLUTION_DATE,LEASE_EXP_DATE,PRESENT_INSTUTION_DTL,ACTION_DTL_CA_SITE_ALLOTTED,PURPOSE_DTL,APPROVED_PLAN_DTL,APPROVED_PLAN_SMRY,CONSTRUCTION_VARIATION,POSTAL_ADDRESS,OTHER_DTL,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY) SELECT ORDER_ID,PURCHASE_ID,AUTHORIZED_PERSON,SITE_ID,LAYOUT_NAME,CA_SITE_NO,ALLOTTEE_ORGANISATION_NAME,DIM_EW,DIM_NS,DIM_TOTAL,DIM_TYPE,EAST,WEST,NORTH,SOUTH,PURPOSE_NAME,DATE_OF_LEASE_AGREEMENT,AMOUNT,LEASE_REGN_NO,BOARD_RES_NUMBER,BOARD_RES_DATE,RESOLUTION_NO,RESOLUTION_DATE,LEASE_EXP_DATE,PRESENT_INSTUTION_DTL,ACTION_DTL_CA_SITE_ALLOTTED,PURPOSE_DTL,APPROVED_PLAN_DTL,APPROVED_PLAN_SMRY,CONSTRUCTION_VARIATION,POSTAL_ADDRESS,OTHER_DTL,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY FROM BDA_PURCHASE_ORDER WHERE ORDER_ID = $1 AND PURCHASE_ID = $2',[input.order_id, input.purchase_id])

                    const q1 = t.none('UPDATE BDA_PURCHASE_ORDER SET STATUS = $1,UPDATED_ON = $2,UPDATED_BY = $3 WHERE ORDER_ID = $4 AND PURCHASE_ID = $5', ['C', input.updated_on, input.created_by, input.order_id, input.purchase_id]);

                    const q2 = t.none('INSERT INTO BDA_PURCHASE_AUDIT_HISTORY(ORDER_ID,PURCHASE_ID,AUTHORIZED_PERSON,SITE_ID,LAYOUT_NAME,CA_SITE_NO,ALLOTTEE_ORGANISATION_NAME,DIM_EW,DIM_NS,DIM_TOTAL,DIM_TYPE,EAST,WEST,NORTH,SOUTH,PURPOSE_NAME,DATE_OF_LEASE_AGREEMENT,AMOUNT,LEASE_REGN_NO,BOARD_RES_NUMBER,BOARD_RES_DATE,RESOLUTION_NO,RESOLUTION_DATE,LEASE_EXP_DATE,PRESENT_INSTUTION_DTL,ACTION_DTL_CA_SITE_ALLOTTED,PURPOSE_DTL,APPROVED_PLAN_DTL,APPROVED_PLAN_SMRY,CONSTRUCTION_VARIATION,POSTAL_ADDRESS,OTHER_DTL,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,REGISTRATION_DOC,PURCHASE_DOC,VERIFIED_PURCHASE_DOC,ORIGINAL_REGISTRATION_DOC,ORIGINAL_PURCHASE_DOC,ORIGINAL_VERIFIED_PURCHASE_DOC,INSERTION_DATE) SELECT ORDER_ID,PURCHASE_ID,AUTHORIZED_PERSON,SITE_ID,LAYOUT_NAME,CA_SITE_NO,ALLOTTEE_ORGANISATION_NAME,DIM_EW,DIM_NS,DIM_TOTAL,DIM_TYPE,EAST,WEST,NORTH,SOUTH,PURPOSE_NAME,DATE_OF_LEASE_AGREEMENT,AMOUNT,LEASE_REGN_NO,BOARD_RES_NUMBER,BOARD_RES_DATE,RESOLUTION_NO,RESOLUTION_DATE,LEASE_EXP_DATE,PRESENT_INSTUTION_DTL,ACTION_DTL_CA_SITE_ALLOTTED,PURPOSE_DTL,APPROVED_PLAN_DTL,APPROVED_PLAN_SMRY,CONSTRUCTION_VARIATION,POSTAL_ADDRESS,OTHER_DTL,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,REGISTRATION_DOC,PURCHASE_DOC,VERIFIED_PURCHASE_DOC,ORIGINAL_REGISTRATION_DOC,ORIGINAL_PURCHASE_DOC,ORIGINAL_VERIFIED_PURCHASE_DOC,NOW() FROM BDA_PURCHASE_ORDER WHERE PURCHASE_ID = $1', [input.purchase_id]);

                    return t.batch([q1, q2])

                })

                if (!finalisedPurchase) return {
                    status: false,
                    message: 'DatabaseError'
                }

                return {
                    status: true,
                    data: {
                        order_id: input.order_id,
                        purchase_id: input.purchase_id,
                        status: input.status
                    },
                    message: 'PurchaseSubmitted'
                }
            } catch (error) {
                console.log('======== Audit Purchase Closed Error Purchase=========',error)
                return {
                    status: false,
                    message: 'InternalError'
                }
            }
        }
    },

    //To insert the purchase order second time form mobile app (step = 3)
    updatePurchaseOrder: async (input) => {
        console.log('[======== Update Existing Purchase Order Mobile App Request ========]', input)
        input.status = 'P';
        input.created_on = new Date();
        // let purchaseOrderId = await db.one("SELECT nextval('bda_purchase_id') AS purchase_id")
        // if (!purchaseOrderId) return {
        //     status: false,
        //     message: 'PurchaseIDSeqErr'
        // }

        // input.new_purchase_id = `BDA/CA/${purchaseOrderId.purchase_id}`
        if(input.new_purchase_copy != null && input.new_registration_copy != null) {
            if(input.original_purchase_doc.split('.').pop() === 'pdf'){
                var decodedPdf = await docHelper.decodeBase64Pdf(input.new_purchase_copy);
                var imageBuffer = await decodedPdf.data;
                var type = await decodedPdf.type
                var fileName = Date.now() + '.' + type;
            }else{
                var decodedImg = await docHelper.decodeBase64Image(input.new_purchase_copy);
                var imageBuffer = await decodedImg.data;
                var type = await decodedImg.type
                var fileName = Date.now() + '.' + type;
            }
    
    
            if(input.original_registration_doc.split('.').pop() === 'pdf') {
                var decodedRegPdf = await docHelper.decodeBase64Pdf(input.new_registration_copy);
                var reg_imageBuffer = await decodedRegPdf.data;
                var type_reg = await decodedRegPdf.type
                var reg_fileName = Date.now() + '.' + type_reg;
            }else{
                var decodedRegImg = await docHelper.decodeBase64Image(input.new_registration_copy);
                var reg_imageBuffer = await decodedRegImg.data;
                var type_reg = await decodedRegImg.type
                var reg_fileName = Date.now() + '.' + type_reg;
            }
    
            try {
                fs.writeFileSync(uploadFolder.identityPath + fileName, imageBuffer, 'base64');
                fs.writeFileSync(uploadFolder.identityPath + reg_fileName, reg_imageBuffer, 'base64');
    
            } catch (err) {
                console.error(err)
                return {
                    status: false,
                    message: 'Failed to upload file'
                }
            }
        }
        
        //need to change the query if file uploading is their
        try {

            let order = await db.tx(t => {

                const q1 = t.none('INSERT INTO BDA_PURCHASE_AUDIT_HISTORY(ORDER_ID,PURCHASE_ID,AUTHORIZED_PERSON,SITE_ID,LAYOUT_NAME,CA_SITE_NO,ALLOTTEE_ORGANISATION_NAME,DIM_EW,DIM_NS,DIM_TOTAL,DIM_TYPE,EAST,WEST,NORTH,SOUTH,PURPOSE_NAME,DATE_OF_LEASE_AGREEMENT,AMOUNT,LEASE_REGN_NO,BOARD_RES_NUMBER,BOARD_RES_DATE,RESOLUTION_NO,RESOLUTION_DATE,LEASE_EXP_DATE,PRESENT_INSTUTION_DTL,ACTION_DTL_CA_SITE_ALLOTTED,PURPOSE_DTL,APPROVED_PLAN_DTL,APPROVED_PLAN_SMRY,CONSTRUCTION_VARIATION,POSTAL_ADDRESS,OTHER_DTL,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,TAG_NAME,REGISTRATION_DOC,PURCHASE_DOC,VERIFIED_PURCHASE_DOC,ORIGINAL_REGISTRATION_DOC,ORIGINAL_PURCHASE_DOC,ORIGINAL_VERIFIED_PURCHASE_DOC,INSERTION_DATE) SELECT ORDER_ID,PURCHASE_ID,AUTHORIZED_PERSON,SITE_ID,LAYOUT_NAME,CA_SITE_NO,ALLOTTEE_ORGANISATION_NAME,DIM_EW,DIM_NS,DIM_TOTAL,DIM_TYPE,EAST,WEST,NORTH,SOUTH,PURPOSE_NAME,DATE_OF_LEASE_AGREEMENT,AMOUNT,LEASE_REGN_NO,BOARD_RES_NUMBER,BOARD_RES_DATE,RESOLUTION_NO,RESOLUTION_DATE,LEASE_EXP_DATE,PRESENT_INSTUTION_DTL,ACTION_DTL_CA_SITE_ALLOTTED,PURPOSE_DTL,APPROVED_PLAN_DTL,APPROVED_PLAN_SMRY,CONSTRUCTION_VARIATION,POSTAL_ADDRESS,OTHER_DTL,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,TAG_NAME,REGISTRATION_DOC,PURCHASE_DOC,VERIFIED_PURCHASE_DOC,ORIGINAL_REGISTRATION_DOC,ORIGINAL_PURCHASE_DOC,ORIGINAL_VERIFIED_PURCHASE_DOC,NOW() FROM BDA_PURCHASE_ORDER WHERE ORDER_ID = $1 AND PURCHASE_ID = $2',[input.order_id, input.purchase_id]);

                const q2 = t.none('UPDATE BDA_PURCHASE_ORDER SET ORDER_ID = $1,AUTHORIZED_PERSON = $2,SITE_ID = $3, LAYOUT_NAME = $4, CA_SITE_NO = $5, ALLOTTEE_ORGANISATION_NAME = $6,DIM_EW = $7, DIM_NS = $8, DIM_TOTAL = $9, DIM_TYPE = $10, EAST = $11, WEST = $12,NORTH = $13, SOUTH = $14, PURPOSE_NAME = $15, DATE_OF_LEASE_AGREEMENT = $16,AMOUNT = $17, LEASE_REGN_NO = $18, BOARD_RES_NUMBER = $19, BOARD_RES_DATE = $20,RESOLUTION_NO = $21, RESOLUTION_DATE = $22, LEASE_EXP_DATE = $23,PRESENT_INSTUTION_DTL = $24, ACTION_DTL_CA_SITE_ALLOTTED = $25, PURPOSE_DTL = $26, APPROVED_PLAN_DTL = $27, APPROVED_PLAN_SMRY = $28, CONSTRUCTION_VARIATION = $29, POSTAL_ADDRESS = $30, OTHER_DTL = $31, STATUS = $32, CREATED_ON = $33,CREATED_BY = $34, UPDATED_ON = $35, UPDATED_BY = $36,REGISTRATION_DOC = $37,PURCHASE_DOC = $38,ORIGINAL_REGISTRATION_DOC = $39,ORIGINAL_PURCHASE_DOC = $40 WHERE ORDER_ID = $41 AND PURCHASE_ID = $42', [Date.now(),input.authorized_person, input.site_id, input.layout_name, input.ca_site_no, input.allottee_organisation_name, input.dim_ew, input.dim_ns, input.dim_total, input.dim_type, input.east, input.west, input.north, input.south, input.purpose_name, input.lease_regn_date, input.amount, input.lease_regn_no, input.board_res_number, input.board_res_date, input.resolution_no, input.resolution_date, input.lease_exp_date, input.present_instution_dtl, input.action_dtl_ca_site_allotted, input.purpose_dtl, input.approved_plan_dtl, input.approved_plan_smry, input.construction_variation, input.postal_address, input.other_dtl, input.status, input.created_on, input.user_id,null, null,reg_fileName,fileName,input.original_registration_doc,input.original_purchase_doc,input.order_id, input.purchase_id]);

                return t.batch([q1, q2])

            })
            if (!order.length) return {
                status: false,
                message: 'No Record Found'
            }

            return {
                status: true,
                data: {
                    status: input.status,
                    order_id: input.order_id,
                    purchase_id: input.new_purchase_id
                },
                message: "Order Placed Successfully"
            }

        } catch (error) {
            console.log('[========== Update Existing Purchase Order Mobile App Error ==========]', error)
            return {
                status: false,
                message: 'Internal Server Error'
            }
        }

    },

    //To get the list of all purchase order for web 
    getPurchaseOrderDetails: async () => {
        try {
            let order = await db.any('SELECT A.ORDER_ID,A.PURCHASE_ID, A.AUTHORIZED_PERSON ,A.SITE_ID ,A.LAYOUT_NAME ,A.CA_SITE_NO ,A.ALLOTTEE_ORGANISATION_NAME ,A.DIM_EW ,A.DIM_NS ,A.DIM_TOTAL ,A.DIM_TYPE ,A.EAST ,A.WEST ,A.NORTH ,A.SOUTH ,A.PURPOSE_NAME ,A.DATE_OF_LEASE_AGREEMENT ,A.AMOUNT ,A.LEASE_REGN_NO ,A.BOARD_RES_NUMBER ,A.BOARD_RES_DATE ,A.RESOLUTION_NO ,A.RESOLUTION_DATE ,A.LEASE_EXP_DATE ,A.PRESENT_INSTUTION_DTL ,A.ACTION_DTL_CA_SITE_ALLOTTED ,A.PURPOSE_DTL ,A.APPROVED_PLAN_DTL ,A.APPROVED_PLAN_SMRY ,A.CONSTRUCTION_VARIATION ,A.POSTAL_ADDRESS ,A.OTHER_DTL ,A.STATUS ,A.CREATED_ON AS PURCHASE_DATE,A.TAG_NAME,A.REGISTRATION_DOC AS UPLOADED_REGISTRATION_COPY_ID,A.ORIGINAL_REGISTRATION_DOC AS UPLOADED_REGISTRATION_COPY,A.PURCHASE_DOC,A.ORIGINAL_PURCHASE_DOC,A.VERIFIED_PURCHASE_DOC,A.ORIGINAL_VERIFIED_PURCHASE_DOC,A.UPDATED_ON AS APPROVED_DATE, B.COMPANY_NAME , B.COMPANY_ADDRESS , B.CONTACT_NO , B.EMAIL , B.REGISTRATION_DOC,D.REGISTRATION_NO AS REGISTRATION_ID,B.CREATED_ON AS REGISTRATION_DATE FROM BDA_PURCHASE_ORDER A INNER JOIN BDA_USER_PROFILE B ON A.CREATED_BY = B.USER_ID INNER JOIN BDA_USER_ORG_MAPPING D ON B.USER_ID = D.USER_ID INNER JOIN BDA_SITE_HIERARCHY_MASTER C ON A.SITE_ID = C.SITE_ID AND A.SITE_ID = D.SITE_ID WHERE A.STATUS = $1 ORDER BY A.CREATED_ON DESC',['C'])
            if (!order) return {
                status: false,
                message: 'NoRecordFound'
            }

            return {
                status: true,
                data: order,
                message: 'Record fetched successfully'
            }
        } catch (error) {
            console.log('[====== Get Purchase Order Details Web Error =======]', error)
            return {
                status: false,
                message: 'InternalError'
            }
        }
    },

    //Pending Purchase details for web 8888888
    getPendingPurchaseOrderDetails:async () => {
        let status = ['R','P','C']
        try {
            let order = await db.any('SELECT A.ORDER_ID, A.PURCHASE_ID, A.AUTHORIZED_PERSON ,A.SITE_ID ,A.LAYOUT_NAME ,A.CA_SITE_NO ,A.ALLOTTEE_ORGANISATION_NAME ,A.DIM_EW ,A.DIM_NS ,A.DIM_TOTAL ,A.DIM_TYPE ,A.EAST ,A.WEST ,A.NORTH ,A.SOUTH ,A.PURPOSE_NAME ,A.DATE_OF_LEASE_AGREEMENT ,A.AMOUNT ,A.LEASE_REGN_NO ,A.BOARD_RES_NUMBER ,A.BOARD_RES_DATE ,A.RESOLUTION_NO ,A.RESOLUTION_DATE ,A.LEASE_EXP_DATE ,A.PRESENT_INSTUTION_DTL ,A.ACTION_DTL_CA_SITE_ALLOTTED ,A.PURPOSE_DTL ,A.APPROVED_PLAN_DTL ,A.APPROVED_PLAN_SMRY ,A.CONSTRUCTION_VARIATION ,A.POSTAL_ADDRESS ,A.OTHER_DTL ,A.STATUS ,A.CREATED_ON AS PURCHASE_DATE,A.TAG_NAME,A.UPDATED_ON,A.REGISTRATION_DOC as UPLOADED_REGISTRATION_COPY_ID,A.original_registration_doc as UPLOADED_REGISTRATION_COPY,A.purchase_doc,A.original_purchase_doc ,A.verified_purchase_doc,A.original_verified_purchase_doc,a.updated_on as approved_date, B.COMPANY_NAME , B.COMPANY_ADDRESS , B.CONTACT_NO , B.EMAIL , B.REGISTRATION_DOC,D.REGISTRATION_NO AS REGISTRATION_ID,B.CREATED_ON AS REGISTRATION_DATE FROM BDA_PURCHASE_ORDER A INNER JOIN BDA_USER_PROFILE B ON A.CREATED_BY = B.USER_ID INNER JOIN BDA_USER_ORG_MAPPING D ON B.USER_ID = D.USER_ID INNER JOIN BDA_SITE_HIERARCHY_MASTER C ON A.SITE_ID = C.SITE_ID AND A.SITE_ID = D.SITE_ID WHERE A.STATUS IN ($1:csv) ORDER BY A.CREATED_ON DESC',[status])
            if (!order) return {
                status: false,
                message: 'NoRecordFound'    
            }

            return {
                status: true,
                data: order,
                message: 'Record fetched successfully'
            }
        } catch (error) {
            console.log('[====== Get Pending Purchase Order Details Web Error =======]', error)
            return {
                status: false,
                message: 'InternalError'
            }
        }
    },

    //Pending Institution Purchase details for web
    getPendingInstitutionPurchaseOrderDetails:async (input) => {
        console.log('===== pending purchase order institution web request =====',input)
        input.status = ['P','R','C'];
        try {
            let order = await db.any('SELECT A.ORDER_ID, A.PURCHASE_ID, A.AUTHORIZED_PERSON ,A.SITE_ID ,A.LAYOUT_NAME ,A.CA_SITE_NO ,A.ALLOTTEE_ORGANISATION_NAME ,A.DIM_EW ,A.DIM_NS ,A.DIM_TOTAL ,A.DIM_TYPE ,A.EAST ,A.WEST ,A.NORTH ,A.SOUTH ,A.PURPOSE_NAME ,A.DATE_OF_LEASE_AGREEMENT ,A.AMOUNT ,A.LEASE_REGN_NO ,A.BOARD_RES_NUMBER ,A.BOARD_RES_DATE ,A.RESOLUTION_NO ,A.RESOLUTION_DATE ,A.LEASE_EXP_DATE ,A.PRESENT_INSTUTION_DTL ,A.ACTION_DTL_CA_SITE_ALLOTTED ,A.PURPOSE_DTL ,A.APPROVED_PLAN_DTL ,A.APPROVED_PLAN_SMRY ,A.CONSTRUCTION_VARIATION ,A.POSTAL_ADDRESS ,A.OTHER_DTL ,A.STATUS ,A.CREATED_ON AS PURCHASE_DATE,A.TAG_NAME, A.UPDATED_ON,A.REGISTRATION_DOC as UPLOADED_REGISTRATION_COPY_ID,A.original_registration_doc as UPLOADED_REGISTRATION_COPY,A.purchase_doc,A.original_purchase_doc ,A.verified_purchase_doc,A.original_verified_purchase_doc,B.COMPANY_NAME , B.COMPANY_ADDRESS , B.CONTACT_NO , B.EMAIL , B.REGISTRATION_DOC FROM BDA_PURCHASE_ORDER A INNER JOIN BDA_USER_PROFILE B ON A.CREATED_BY = B.USER_ID INNER JOIN BDA_SITE_HIERARCHY_MASTER C ON A.SITE_ID = C.SITE_ID WHERE A.STATUS IN ($1:csv) AND B.USER_ID = $2 ORDER BY A.CREATED_ON DESC',[input.status, input.user_id])
            if (!order) return {
                status: false,
                message: 'NoRecordFound'
            }

            return {
                status: true,
                data: order,
                message: 'RecordFetchedSuccessfully'
            }
        } catch (error) {
            console.log('[====== Get Pending Purchase Order Details Web Error =======]', error)
            return {
                status: false,
                message: 'InternalError'
            }
        }
    },


    //To get institution purchase order details (step = 4)
    getInstitutionPurchaseDetails: async (input) => {
        console.log('[====== Get institution purchase order details Request =======]', input)
        try {
            let order = await db.any('SELECT A.ORDER_ID, A.PURCHASE_ID, A.AUTHORIZED_PERSON, A.SITE_ID, A.LAYOUT_NAME, A.CA_SITE_NO, A.ALLOTTEE_ORGANISATION_NAME, A.DIM_EW, A.DIM_NS, A.DIM_TOTAL, A.DIM_TYPE, A.EAST, A.WEST, A.NORTH, A.SOUTH, A.PURPOSE_NAME, A.DATE_OF_LEASE_AGREEMENT, A.AMOUNT, A.LEASE_REGN_NO, A.BOARD_RES_NUMBER, A.BOARD_RES_DATE, A.RESOLUTION_NO, A.RESOLUTION_DATE, A.LEASE_EXP_DATE, A.PRESENT_INSTUTION_DTL, A.ACTION_DTL_CA_SITE_ALLOTTED, A.PURPOSE_DTL, A.APPROVED_PLAN_DTL, A.APPROVED_PLAN_SMRY, A.CONSTRUCTION_VARIATION, A.POSTAL_ADDRESS, A.OTHER_DTL, A.STATUS AS PURCHASE_STATUS, A.CREATED_ON AS PURCHASE_REQ_DATE, A.TAG_NAME,A.UPDATED_ON,A.REGISTRATION_DOC AS UPLOADED_REGISTRATION_COPY_ID,A.ORIGINAL_REGISTRATION_DOC AS UPLOADED_REGISTRATION_COPY,A.PURCHASE_DOC,A.VERIFIED_PURCHASE_DOC,A.ORIGINAL_PURCHASE_DOC,A.ORIGINAL_VERIFIED_PURCHASE_DOC, B.DIV_NAME, B.LEASE_PERIOD, B.NEXT_LEASE_RENEWAL_DATE, B.LUMPSUM_ANNUITY, B.LEASE_REGN_DATE, B.RENEWAL_LEASE_AMOUNT, C.SITE_NAME, D.COMPANY_NAME, D.CONTACT_NO, D.EMAIL, D.COMPANY_ADDRESS, D.REGISTRATION_DOC,D.ORIGINAL_FILE,C.REGISTRATION_NO AS REGISTRATION_ID,D.CREATED_ON AS REGISTRATION_DATE FROM BDA_PURCHASE_ORDER A INNER JOIN BDA_SITE_HIERARCHY_MASTER B ON A.SITE_ID = B.SITE_ID INNER JOIN BDA_USER_ORG_MAPPING C ON B.SITE_ID = C.SITE_ID INNER JOIN BDA_USER_PROFILE D ON C.USER_ID = D.USER_ID AND D.USER_ID = $1 ORDER BY A.CREATED_ON DESC', [input.user_id])
            if (!order) return {
                status: false,
                message: 'NoRecordFound'
            }
            return {
                status: true,
                data: order,
                message: 'RecordsFetched'
            }
        } catch (error) {
            console.log('[====== Get institution purchase order details Error =======]', error)
            return {
                status: false,
                message: 'InternalError'
            }
        }
    },

    postUploadSignedCopy: async (input) => {
        console.log('[====== Upload signed purchase details  =======]', input)

            //For image handling
            if(input.file_type === 'pdf'){
                var decodedPdf = await docHelper.decodeBase64Pdf(input.upload_img);
                var imageBuffer = await decodedPdf.data;
                var type = await decodedPdf.type;
                var fileName = Date.now() + '.' + type;

            }else{
                var decodedImg = await docHelper.decodeBase64Image(input.upload_img);
                var imageBuffer = await decodedImg.data;
                var type = await decodedImg.type;
                var fileName = Date.now() + '.' + type;
            }

            //For uploading image
            try {
                fs.writeFileSync(uploadFolder.identityPath + fileName, imageBuffer, 'base64');
            } catch (err) {
                console.error(err)
                return {
                    status: false,
                    message: 'Failed to upload file'
                }
            }
        try {
            let order = await db.none('update bda_purchase_order set verified_purchase_doc = $1,original_verified_purchase_doc = $2 where purchase_id = $3 and site_id = $4', [fileName,input.original_verified_purchase_doc,input.purchase_id,input.site_id])

            return {
                status: true,
                data: order,
                message: 'success'
            }
        } catch (error) {
            console.log('[====== Get institution purchase order details Error =======]', error)
            return {
                status: false,
                message: 'InternalError'
            }
        }
    },

    //institution purchase timeline history (step = 5)
    getInstitutionPurchaseTimline:async(input) => {
        console.log('[====== Purchase timeline history Request ======]',input)
        try {
            let timeline = await db.any('SELECT A.SEQ_ID,A.PURCHASE_ID,A.ORDER_ID,A.AUTHORIZED_PERSON, A.SITE_ID, A.LAYOUT_NAME, A.CA_SITE_NO, A.ALLOTTEE_ORGANISATION_NAME, A.DIM_EW, A.DIM_NS, A.DIM_TOTAL, A.DIM_TYPE, A.EAST, A.WEST, A.NORTH, A.SOUTH, A.PURPOSE_NAME, A.DATE_OF_LEASE_AGREEMENT, A.AMOUNT, A.LEASE_REGN_NO, A.BOARD_RES_NUMBER, A.BOARD_RES_DATE, A.RESOLUTION_NO, A.RESOLUTION_DATE, A.LEASE_EXP_DATE, A.PRESENT_INSTUTION_DTL, A.ACTION_DTL_CA_SITE_ALLOTTED, A.PURPOSE_DTL, A.APPROVED_PLAN_DTL, A.APPROVED_PLAN_SMRY, A.CONSTRUCTION_VARIATION, A.POSTAL_ADDRESS, A.OTHER_DTL, A.STATUS AS PURCHASE_STATUS, A.CREATED_ON AS PURCHASE_REQ_DATE, A.TAG_NAME,A.UPDATED_ON,A.INSERTION_DATE, B.DIV_NAME, B.LEASE_PERIOD, B.NEXT_LEASE_RENEWAL_DATE, B.LUMPSUM_ANNUITY, B.LEASE_REGN_DATE, B.RENEWAL_LEASE_AMOUNT, C.SITE_NAME, D.COMPANY_NAME, D.CONTACT_NO, D.EMAIL, D.COMPANY_ADDRESS, D.REGISTRATION_DOC FROM BDA_PURCHASE_AUDIT_HISTORY A INNER JOIN BDA_SITE_HIERARCHY_MASTER B ON A.SITE_ID = B.SITE_ID INNER JOIN BDA_USER_ORG_MAPPING C ON B.SITE_ID = C.SITE_ID INNER JOIN BDA_USER_PROFILE D ON C.USER_ID = D.USER_ID AND A.PURCHASE_ID = $1 AND D.USER_ID = $2 ORDER BY A.SEQ_ID DESC',[input.purchase_id,input.user_id]);
            if(!timeline) return {
                status: false,
                message:'NoOrderHistory'
            }
            return {
                status: true,
                data: timeline,
                message:'RecordsFetched'
            }
        } catch (error) {
            console.log('[====== Purchase timeline history Error ======]',error)
            return {
                status: false,
                message:'InternalError'
            }
        }
    },

    //Web-portal timeline history
    getBDAPurchaseTimeline:async(input) => {
        console.log('[======== BDA portal timline history Request =========]',input)
        try {
            let timeline = await db.any('SELECT A.*,B.COMPANY_NAME,B.CONTACT_NO,B.EMAIL,B.COMPANY_ADDRESS FROM BDA_PURCHASE_AUDIT_HISTORY A, BDA_USER_PROFILE B WHERE A.CREATED_BY = B.USER_ID AND A.PURCHASE_ID = $1 ORDER BY A.SEQ_ID DESC',[input.purchase_id]);
            if(!timeline) return {
                status: false,
                message: 'NoOrderHistory'
            }
            return {
                status: true,
                data:timeline,
                message:'RecordFetched'
            }
        } catch (error) {
            console.log('[====== Purchase timeline history Error ======]',error)
            return {
                status: false,
                message:'InternalError'
            }
        }
    },

    getInstitutionPurchaseDetailsMobile: async (input) => {
        console.log('[====== Get institution purchase order details Request =======]', input)
        try {
            let order = await db.one('SELECT A.ORDER_ID, A.PURCHASE_ID, A.AUTHORIZED_PERSON, A.SITE_ID, A.LAYOUT_NAME, A.CA_SITE_NO, A.ALLOTTEE_ORGANISATION_NAME, A.DIM_EW, A.DIM_NS, A.DIM_TOTAL, A.DIM_TYPE, A.EAST, A.WEST, A.NORTH, A.SOUTH, A.PURPOSE_NAME, A.DATE_OF_LEASE_AGREEMENT, A.AMOUNT, A.LEASE_REGN_NO, A.BOARD_RES_NUMBER, A.BOARD_RES_DATE, A.RESOLUTION_NO, A.RESOLUTION_DATE, A.LEASE_EXP_DATE, A.PRESENT_INSTUTION_DTL, A.ACTION_DTL_CA_SITE_ALLOTTED, A.PURPOSE_DTL, A.APPROVED_PLAN_DTL, A.APPROVED_PLAN_SMRY, A.CONSTRUCTION_VARIATION, A.POSTAL_ADDRESS, A.OTHER_DTL, A.STATUS AS PURCHASE_STATUS, A.CREATED_ON AS PURCHASE_REQ_DATE, A.TAG_NAME,A.UPDATED_ON, B.DIV_NAME, B.LEASE_PERIOD, B.NEXT_LEASE_RENEWAL_DATE, B.LUMPSUM_ANNUITY, B.LEASE_REGN_DATE, B.RENEWAL_LEASE_AMOUNT, C.SITE_NAME, D.COMPANY_NAME, D.CONTACT_NO, D.EMAIL, D.COMPANY_ADDRESS, D.REGISTRATION_DOC FROM BDA_PURCHASE_ORDER A INNER JOIN BDA_SITE_HIERARCHY_MASTER B ON A.SITE_ID = B.SITE_ID INNER JOIN BDA_USER_ORG_MAPPING C ON B.SITE_ID = C.SITE_ID INNER JOIN BDA_USER_PROFILE D ON C.USER_ID = D.USER_ID AND A.ORDER_ID = $1 AND D.USER_ID = $2 ORDER BY A.CREATED_ON DESC', [input.order_id,input.user_id])
            if (!order) return {
                status: false,
                message: 'NoRecordFound'
            }
            return {
                status: true,
                data: order,
                message: 'RecordsFetched'
            }
        } catch (error) {
            console.log('[====== Get institution purchase order details Error =======]', error)
            return {
                status: false,
                message: 'InternalError'
            }
        }
    },

}