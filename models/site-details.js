'use strict';

const db = require('../config/db').db;
const moment = require('moment')

module.exports = {

    //Service to get the list of available site details for mobile app
    /*SELECT SITE_ID, DIV_NAME, LAYOUT_NAME, CA_SITE_NO, ALLOTTEE_ORGANISATION_NAME, DIM_TOTAL, DIM_TYPE, EAST, WEST, NORTH, SOUTH, PURPOSE_NAME, DATE_OF_LEASE_AGREEMENT, LEASE_PERIOD, NEXT_LEASE_RENEWAL_DATE, LUMPSUM_ANNUITY, LEASE_REGN_NO, AMOUNT, LEASE_REGN_DATE, RENEWAL_LEASE_AMOUNT FROM BDA_SITE_HIERARCHY_MASTER WHERE STATUS = $1 ORDER BY CREATED_ON DESC*/
    
    siteDetails: async () => {
        try {
            let siteDtls = await db.any('SELECT A.SITE_ID, A.DIV_NAME, A.LAYOUT_NAME, A.CA_SITE_NO, A.ALLOTTEE_ORGANISATION_NAME, A.DIM_TOTAL, A.DIM_TYPE, A.EAST, A.WEST, A.NORTH, A.SOUTH, A.PURPOSE_NAME, A.DATE_OF_LEASE_AGREEMENT, A.LEASE_PERIOD, A.NEXT_LEASE_RENEWAL_DATE, A.LUMPSUM_ANNUITY, A.LEASE_REGN_NO, A.AMOUNT, A.LEASE_REGN_DATE, A.RENEWAL_LEASE_AMOUNT, B.STATUS FROM BDA_SITE_HIERARCHY_MASTER A LEFT OUTER JOIN BDA_USER_ORG_MAPPING B ON A.SITE_ID = B.SITE_ID ORDER BY A.CREATED_ON DESC')
            if (!siteDtls) return {
                status: false,
                message: 'No data found!!'
            }

            siteDtls.map((item)=>{
                if(item.status == 'N' || item.status == 'Y'){
                    item.disabled = true
                }
            })

            return {
                status: true,
                data: siteDtls,
                message: 'Records fetched successfully!'
            }
        } catch (error) {
            console.log('[========= Get Available Site Details Mobile App Error =======]', error);
            return {
                status: false,
                message: 'Internal error! Please try again'
            }
        }
    },

    //Service to get the site dimensions details for mobile app on site id
    siteSummaryDetails: async (input) => {
        console.log('[==== Get Site Dimensions Details Mobile App Request =====]', input)
        try {
            let siteSummaryDtls = await db.one('SELECT EAST, WEST, NORTH, SOUTH, PURPOSE_NAME, DATE_OF_LEASE_AGREEMENT, LEASE_PERIOD, NEXT_LEASE_RENEWAL_DATE, LUMPSUM_ANNUITY, LEASE_REGN_NO, AMOUNT, LEASE_REGN_DATE, RENEWAL_LEASE_AMOUNT FROM BDA_SITE_HIERARCHY_MASTER WHERE SITE_ID = $1 AND STATUS = $2', [input.site_id, 'Y'])
            if (!siteSummaryDtls) return {
                status: false,
                message: 'No data found!!'
            }

            return {
                status: true,
                data: siteSummaryDtls,
                message: 'Records fetched successfully!'
            }
        } catch (error) {
            console.log('[==== Get Site Dimensions Details Mobile App Error ====]', error);
            return {
                status: false,
                message: 'Internal error! Please try again'
            }
        }
    },

    //Service to get the site id and site name for mobile app
    getAvailableSite: async() => {
        try {
            let availableSite = await db.one('SELECT SITE_ID,ALLOTTEE_ORGANISATION_NAME FROM BDA_SITE_HIERARCHY_MASTER WHERE STATUS = $1 ORDER BY CREATED_ON DESC',['Y'])
            if(!availableSite) return {
                status: false,
                message: 'No data found'
            }
            return {
                status: true,
                data: availableSite,
                message: 'Records fetched successfully'
            }
        } catch (error) {
            console.log('[=== siteSummaryDetails ERR ===]', error);
            return {
                status: false,
                message: 'Internal error! Please try again'
            }
        }
    },

    //Service to get the user site details for mobile app
    getUserSiteDetails: async(input) => {
        console.log('[==== To Get User Site Details Mobile App Request ====]',input);
        try {
            let userSite = await db.any('SELECT A.USER_ID, A.COMPANY_NAME, A.CONTACT_NO, A.EMAIL, A.COMPANY_ADDRESS, B.SITE_ID, B.SITE_NAME AS ALLOTTEE_ORGANISATION_NAME, B.CA_SITE_NO, B.SITE_LAYOUT_NAME AS LAYOUT_NAME, B.SITE_DIMENSION AS DIM_TOTAL, B.PURPOSE AS PURPOSE_NAME, B.STATUS, B.CREATED_ON as REGISTRATION_DATE, B.updated_on as APPROVED_DATE, B.registration_doc as REGISTRATION_DOC_ID, B.original_registration_doc as REGISTRATION_DOC_NAME, B.registration_no as REG_ID, B.remark as APPROVAL_REMARK, B.SITE_FLAG, C.COMP_FILE_NO, C.DIV_NAME, C.SUB_LAYOUT_NAME, C.DIM_EW, C.DIM_NS, C.DIM_TYPE, C.EAST, C.WEST, C.NORTH, C.SOUTH, C.DATE_OF_LEASE_AGREEMENT, C.LEASE_PERIOD, C.NEXT_LEASE_RENEWAL_DATE, C.LUMPSUM_ANNUITY, C.AMOUNT, C.LEASE_REGN_NO, C.LEASE_REGN_DATE, C.BOARD_RES_NUMBER, C.BOARD_RES_DATE, C.COURT_NAME, C.CASE_TYPE, C.CASE_NO, C.CASE_YEAR, C.LEASE_EXTENSION_PERIOD, C.LEASE_RENEWAL_COMMENCING_FROM, C.LEASE_RENEWAL_REG_NO, C.LEASE_RENEWAL_REG_DATE, C.RENEWAL_ORDER_NO, C.RENEWAL_ORDER_DATE, C.LUMPSUM_ANNUITY_RENEWAL, C.RENEWAL_LEASE_AMOUNT, C.CHALLAN_NO, C.CHALLAN_DATE, C.REMARKS, D.FNAME as APPROVER_FNAME, D.LNAME as APPROVER_LNAME FROM BDA_USER_PROFILE A INNER JOIN BDA_USER_ORG_MAPPING B ON A.USER_ID = B.USER_ID LEFT OUTER JOIN BDA_SITE_HIERARCHY_MASTER C ON B.SITE_ID = C.SITE_ID left outer join (select USER_ID,FNAME,LNAME from BDA_EMPLOYEE_PROFILE)D on B.updated_by = D.USER_ID WHERE B.USER_ID = $1 AND B.STATUS IN ($2:csv) AND B.SITE_FLAG = $3 ',[input.user_id,['Y','R','N'], 'M'])
            console.log(userSite);
            if(!userSite) return {
                status: false,
                message: 'No data found'
            }
            return {
                status: true,
                data:userSite,
                message: 'Records fetched successfully'
            }
        } catch (error) {
            console.log('[==== To Get User Site Details Mobile App Error ====]', error);
            return {
                status: false,
                message: 'Internal error! Please try again'
            }
        }
    },

    getSiteMasterDataForUser:async(input) => {
        console.log('[============== getSiteMasterDataForUser Req ==============]',input)
        try {
            let getSiteInfo = await db.any('SELECT A.SITE_ID, A.DIV_NAME, A.LAYOUT_NAME, A.CA_SITE_NO, A.ALLOTTEE_ORGANISATION_NAME, A.DIM_TOTAL, A.DIM_TYPE, A.EAST, A.WEST, A.NORTH, A.SOUTH, A.PURPOSE_NAME, A.DATE_OF_LEASE_AGREEMENT, A.LEASE_PERIOD, A.NEXT_LEASE_RENEWAL_DATE, A.LUMPSUM_ANNUITY, A.LEASE_REGN_NO, A.AMOUNT, A.LEASE_REGN_DATE, A.RENEWAL_LEASE_AMOUNT, B.STATUS FROM BDA_SITE_HIERARCHY_MASTER A LEFT OUTER JOIN BDA_USER_ORG_MAPPING B ON A.SITE_ID = B.SITE_ID ORDER BY A.SITE_ID, A.CREATED_ON DESC')
            if(!getSiteInfo){
                return {
                    status:false,
                    message:'NoRecordFound'
                }
            }else{
                getSiteInfo.map((item)=>{
                    if(item.status == 'N' || item.status == 'Y'){
                        item.disabled = true
                    }
                })
                
                console.log('-------------',getSiteInfo.length)
                return {
                    status:true,
                    data:getSiteInfo,
                    message:'Success'
                }
            }
        } catch (error) {
            console.log('[========== getSiteMasterDataForUser Err ==========]',error)
            return {
                status:true,
                message:'InternalServerError'
            }
        }
    },

    //get the list of site which are not in screening phase
    getNotScreeningPhaseSite:async(input) => {
        console.log('[==== getNotScreeningPhaseSite Req ====]',input)
        try {
            let site = await db.any('SELECT REG_ID, COMPANY_NAME, CONTACT_NO, EMAIL, COMPANY_ADDRESS, REGISTRATION_DOC, REGISTRATION_DOC_NAME, SITE_ID, CA_SITE_NO, SITE_DIMENSION, SITE_FLAG, COMP_FILE_NO, DIV_NAME, LAYOUT_NAME, SUB_LAYOUT_NAME, ALLOTTEE_ORGANISATION_NAME, DIM_EW, DIM_NS, DIM_TOTAL, DIM_TYPE, EAST, WEST, NORTH, SOUTH, PURPOSE_NAME, DATE_OF_LEASE_AGREEMENT, LEASE_PERIOD, NEXT_LEASE_RENEWAL_DATE, LUMPSUM_ANNUITY, AMOUNT, LEASE_REGN_NO, LEASE_REGN_DATE, BOARD_RES_NUMBER, BOARD_RES_DATE, COURT_NAME, CASE_TYPE, CASE_NO, CASE_YEAR, LEASE_EXTENSION_PERIOD, LEASE_RENEWAL_COMMENCING_FROM, LEASE_RENEWAL_REG_NO, LEASE_RENEWAL_REG_DATE, RENEWAL_ORDER_NO, RENEWAL_ORDER_DATE, LUMPSUM_ANNUITY_RENEWAL, RENEWAL_LEASE_AMOUNT, CHALLAN_NO, CHALLAN_DATE, REMARKS FROM ( SELECT A.COMPANY_NAME, A.CONTACT_NO, A.EMAIL, A.COMPANY_ADDRESS, B.SITE_ID, B.SITE_NAME AS ALLOTTEE_ORGANISATION_NAME, B.SITE_DIMENSION, B.REGISTRATION_NO AS REG_ID, B.CREATED_ON as registration_date, B.SITE_FLAG, B.REGISTRATION_DOC, B.ORIGINAL_REGISTRATION_DOC AS REGISTRATION_DOC_NAME, C.COMP_FILE_NO, C.DIV_NAME, C.LAYOUT_NAME, C.SUB_LAYOUT_NAME, C.CA_SITE_NO, C.DIM_EW, C.DIM_NS, C.DIM_TOTAL, C.DIM_TYPE, C.EAST, C.WEST, C.NORTH, C.SOUTH, C.PURPOSE_NAME, C.DATE_OF_LEASE_AGREEMENT, C.LEASE_PERIOD, C.NEXT_LEASE_RENEWAL_DATE, C.LUMPSUM_ANNUITY, C.AMOUNT, C.LEASE_REGN_NO, C.LEASE_REGN_DATE, C.BOARD_RES_NUMBER, C.BOARD_RES_DATE, C.COURT_NAME, C.CASE_TYPE, C.CASE_NO, C.CASE_YEAR, C.LEASE_EXTENSION_PERIOD, C.LEASE_RENEWAL_COMMENCING_FROM, C.LEASE_RENEWAL_REG_NO, C.LEASE_RENEWAL_REG_DATE, C.RENEWAL_ORDER_NO, C.RENEWAL_ORDER_DATE, C.LUMPSUM_ANNUITY_RENEWAL, C.RENEWAL_LEASE_AMOUNT, C.CHALLAN_NO, C.CHALLAN_DATE, C.REMARKS FROM BDA_USER_PROFILE A INNER JOIN BDA_USER_ORG_MAPPING B ON A.USER_ID = B.USER_ID INNER JOIN BDA_SITE_HIERARCHY_MASTER C ON B.SITE_ID = C.SITE_ID WHERE A.USER_ID = $1 AND B.STATUS = $2 ) A WHERE A.SITE_ID NOT IN ( SELECT SITE_ID FROM BDA_PURCHASE_ORDER )',[input.user_id,'Y'])
            if(!site){
                return {
                    status:false,
                    message:'NoRecordFound'
                }
            }else{

                site.map((item) => {
                    if(item.date_of_lease_agreement == 'N/A' || item.date_of_lease_agreement == null || item.date_of_lease_agreement == ''){
                        item.f_date_of_lease_agreement = null
                    }else{
                        let q_date_of_lease_agreement = item.date_of_lease_agreement.split('.')
                        let parts = `${q_date_of_lease_agreement[2]}-${q_date_of_lease_agreement[1]}-${q_date_of_lease_agreement[0]}`
                        item.f_date_of_lease_agreement = moment.utc(new Date(parts), 'YYYY-MM-DD', true).format()
                    }
                    if(item.lease_regn_date == 'N/A' || item.lease_regn_date == null || item.lease_regn_date == ''){
                        item.f_lease_regn_date = null
                    }else{
                        let q_lease_regn_date = item.lease_regn_date.split('.')
                        let parts = `${q_lease_regn_date[2]}-${q_lease_regn_date[1]}-${q_lease_regn_date[0]}`
                        item.f_lease_regn_date = moment.utc(new Date(parts), 'YYYY-MM-DD', true).format()
                    }
                    if(item.board_res_date == 'N/A' || item.board_res_date == null || item.board_res_date == ''){
                        item.f_board_res_date = null
                    }else{
                        let q_board_res_date = item.board_res_date.split('.')
                        let parts = `${q_board_res_date[2]}-${q_board_res_date[1]}-${q_board_res_date[0]}`
                        
                         console.log('parts ===>', parts,item.board_res_date)
                         item.f_board_res_date = moment.utc(new Date(parts), 'YYYY-MM-DD', true).format()
                        
                    }
                })
                return {
                    status:true,
                    data:site,
                    message:'success'
                }
            }
        } catch (error) {
            console.log('[==== getNotScreeningPhaseSite Err ====]',error)
            return {
                status:false,
                message:'InternalServerError'
            }
        }
    }
}