const db = require('../config/db').db;
const passwordHandler = require('../utils/password-handler');
const jwtHandler = require('../utils/jwt-handler');
const docHelper = require('../utils/image-handler');
const uploadFolder = require('../config/constant');
const SendOtp = require('sendotp-promise');
const msg91Conf = require('../config/constant').msg91;
const fs = require('fs');
const Notification = require("../utils/fcm-notification");
const SendEmail = require('../utils/send-email');
const TriggerMail = require('../utils/send-email');
const sendOtp = new SendOtp(msg91Conf.MSG91_AUTH_KEY, '<#> Your otp is {{otp}} 6qZ3a6t7wQG');
sendOtp.setOtpExpiry('180');

module.exports = {

    //Service for mobile app login
    login: async (input) => {
        console.log('[====== Mobile App Login Request ======]',input);
        try {
            input.email = input.email.toLowerCase();
            let user = await db.one('SELECT A.USER_ID,A.COMPANY_NAME,A.CONTACT_NO,A.EMAIL,A.USER_PASSWORD,A.COMPANY_ADDRESS,B.STATUS,A.USER_TYPE,A.CREATED_ON FROM BDA_USER_PROFILE A INNER JOIN BDA_USER_ORG_MAPPING B ON A.USER_ID = B.USER_ID WHERE A.EMAIL = $1 AND B.STATUS = $2 LIMIT 1',[input.email, 'Y'])
            if (!user) return {
                status: false,
                message: 'User not found!'
            };

            //For password validation
            let isValidPassword = await passwordHandler.comparePassword(input.user_password, user.user_password);
            console.log('[== User Entered Password Mobile App ==]', input.user_password)
            if (!isValidPassword) return {
                status: false,
                message: 'Invalid password!'
            }

            let token = await jwtHandler.signToken(user);
            return {
                status: true,
                message: 'Valid User!!',
                data: {
                    user_id: user.user_id,
                    company_name: user.company_name,
                    email: user.email,
                    contact_no: user.contact_no,
                    address: user.company_address,
                    user_role: user.user_type,
                    registration_doc: user.registration_doc,
                    status: user.status,
                    created_on: user.created_on
                },
                token: token
            }

        } catch (error) {
            console.log('[====== Mobile App Login Error ======]',error);
            console.log(error);
            return {
                status: false,
                message: 'Please try again',
            }
        }
    },

    //Institution Registration from outside the application
    /*registerUser: async (user) => {
        console.log('[====== Institution Registration Request ======]',user);
        try {

            let checkUser = await db.one('SELECT COUNT(*) AS USER_CNT FROM BDA_USER_PROFILE WHERE EMAIL = $1 AND STATUS = $2',[user.email.toLowerCase(),'Y'])
            console.log('[=====User Email count===]',checkUser)
            if(checkUser.user_cnt > 0) return {
                status: false,
                message: 'User already exists'
            }

            //For password validation
            user.email = user.email.toLowerCase();
            user.user_password = await passwordHandler.encodePassword(user.user_password);
            if (!user.user_password) return {
                status: false,
                message: 'Please try again!!'
            }

            //For image handling
            if(user.fileType === 'application/pdf'){
                var decodedPdf = await docHelper.decodeBase64Pdf(user.registration_doc);
                var imageBuffer = await decodedPdf.data;
                var type = await decodedPdf.type;
                var fileName = Date.now() + '.' + type;

            }else{
                var decodedImg = await docHelper.decodeBase64Image(user.registration_doc);
                var imageBuffer = await decodedImg.data;
                var type = await decodedImg.type;
                var fileName = Date.now() + '.' + type;
            }

            try {
                fs.writeFileSync(uploadFolder.identityPath + fileName, imageBuffer, 'base64');
            } catch (err) {
                console.error(err)
                return {
                    status: false,
                    message: 'Failed to upload file'
                }
            }

            //let form_id = Date.now();
            //Registration ID Sequence YYYY-MM-DD-HH-MM-SS
            let generateFormID = new Date()
            let h = `${generateFormID.getHours()}`.padStart(2, '0')
            let m = `${generateFormID.getMinutes()}`.padStart(2, '0')
            let s = `${generateFormID.getSeconds()}`.padStart(2, '0')
            let form_id = `${generateFormID.getFullYear()}${generateFormID.getMonth() + 1}${generateFormID.getDate()}${h}${m}${s}`

            let selectedOrganization = user.alloted_organization;
            console.log('selectedOrganization',selectedOrganization)
            let rawOrganizationList = await selectedOrganization.map(async (item) => {
                console.log('[==im here==]')
                if(item.site_id === null || item.site_id === 'null'){
                    let customSiteId = await db.one("SELECT NEXTVAL('BDA_SITE_ID') AS new_site_id");
                    console.log('[==== SITE ID FOR CUSTOM SITE ====]',customSiteId)
                    item.site_id =  customSiteId.new_site_id;
                }
                console.log('item',item)
                return item;
            })
            console.log('rawOrganizationList',rawOrganizationList)

            const finalOrganizationListArr = await Promise.all(rawOrganizationList)


            console.log('[===== finalOrganizationListArr On Register Mobile App ====]',JSON.stringify(finalOrganizationListArr))


            let userStatus = await db.tx(t => {

                const q1 = t.one('INSERT INTO BDA_USER_PROFILE(USER_ID,COMPANY_NAME,CONTACT_NO,EMAIL,USER_PASSWORD,COMPANY_ADDRESS,STATUS,USER_TYPE,CREATED_ON,CREATED_BY)VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)',[user.user_id,user.company_name,user.contact_no, user.email, user.user_password,user.company_address,'N','INSTITUTION',user.created_on,user.user_id])

                

                const q1 = t.one('INSERT INTO BDA_USER_REGISTRATION(FORM_ID,COMPANY_NAME,ALLOTED_ORGANIZATION,CONTACT_NO,EMAIL,USER_PASSWORD,COMPANY_ADDRESS,REGISTRATION_DOC,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,ORIGINAL_FILE) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13,$14) RETURNING FORM_ID', [form_id, user.company_name, JSON.stringify(finalOrganizationListArr), user.contact_no, user.email, user.user_password, user.company_address, fileName, 'N', new Date(), user.created_by, new Date(), user.updated_by,user.original_file]);

                const q2 = t.none('INSERT INTO BDA_FCM_NOTIFICATION(USER_ID,TOKEN,STATUS,CREATED_ON) VALUES($1,$2,$3,$4)', [form_id, user.token, 'Y', new Date()]);

                return t.batch([q1, q2]);

            })

            let notificationData = {
                title: 'Thank you for getting in touch!',
                body:`Hello ${user.company_name},\nWe have received your Request for Registration ID: #${form_id}.`,
                token:user.token,
                email: user.email,
                template_id:21089981,
                form_id:form_id,
                company_name:user.company_name
            }

            let token = await Notification.sendNotification(notificationData)
            if(!token) return {
                status: false,
                message: 'Failed to send notification'
            }

            if (!userStatus.length) return {
                status: false,
                message: 'Failed to register'
            }

            let email = await TriggerMail.sendMail(notificationData)
            if(!email) return {
                status: false,
                message: 'Failed to send email'
            }

            return {
                status: true,
                data: userStatus[0].form_id,
                message: 'User registered successfully!!'
            }

        } catch (error) {
            console.log('[====== Mobile App Register Error ======]',error);
            return {
                status: false,
                message: 'Please try again!!'
            }
        }
    },*/

    //Service for generating form_id i.e user_id once approved form web-portal for mobile app
    createUserID: async () => {
        try {
            let userID = await db.one("SELECT nextval('bda_user_id_seq')");

            if (!userID) return {
                status: false,
                message: 'Failed to generate user ID'
            }

            return {
                status: true,
                data: userID.nextval,
                message: 'UserID generated successfully!!'
            }

        } catch (error) {
            console.log('[====== User ID Generation Error ======]',error);
            return {
                status: false,
                message: 'Please try again!!'
            }
        }
    },

    //Service for employee regesitration for web
    registerEmployee: async (user) => {
        console.log('[==== Web Portal Register Employee Request ====]', user)
        console.log(user.email);
        console.log(user.fname);

        try {

            //user.email = user.email.toLowerCase();

            user.user_password = await passwordHandler.encodePassword(user.user_password);
            if (!user.user_password) return {
                status: false,
                message: 'Please try again!!'
            }

            let userStatus = await db.one('INSERT INTO bda_employee_profile(user_id,fname,lname,email,user_password,contact_no,status,created_by,created_on,updated_by,updated_on) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING user_id', [user.user_id, user.fname, user.lname, user.email, user.user_password, user.contact_no, 'Y', 10001, new Date(), 10001, new Date()])

            if (!userStatus) return {
                status: false,
                message: 'Failed to register!!'
            }

            return {
                status: true,
                data: userStatus,
                message: 'User registered successfully!!'
            }
        } catch (error) {
            console.log('[==== Web Portal Register Employee Error ====]',error);
            return {
                status: false,
                message: 'InternalError'
            }
        }
    },

    //Service for employee login on web-portal
    loginEmployee: async (input) => {
        console.log('[======= Web Portal Login Employee =======]',input);
        try {
            input.email = input.email.toLowerCase();
            let user = await db.one('SELECT USER_ID,EMP_CODE,FNAME,LNAME,EMAIL,USER_PASSWORD,CONTACT_NO,USER_ROLE,STATUS,CREATED_BY,CREATED_ON FROM BDA_EMPLOYEE_PROFILE WHERE EMAIL = $1 AND STATUS = $2', [input.email, 'Y'])
            if (!user) return {
                status: false,
                message: 'User not found!'
            };

            //For password validation
            let isValidPassword = await passwordHandler.comparePassword(input.user_password, user.user_password);
            console.log('[==== Web User Entered Password ====]', input.user_password)
            if (!isValidPassword) return {
                status: false,
                message: 'Invalid password!'
            }

            let token = await jwtHandler.signToken(user);
            return {
                status: true,
                message: 'Valid User!!',
                data: {
                    user_id: user.user_id,
                    emp_code: user.emp_code,
                    fname: user.fname,
                    lname: user.lname,
                    email: user.email,
                    contact_no: user.contact_no,
                    user_role: user.user_role,
                    address: user.company_address,
                    status: user.status,
                    created_on: user.created_on
                },
                token: token
            }

        } catch (error) {
            console.log('[======= Web Portal Login Error =======]',error);
            return {
                status: false,
                message: 'Please try again',
            }
        }
    },

    //Service for sending OTP for mobile app
    sendOtpToMobile: async (mobile) => {
        console.log('[==== Send OTP Request ====]',mobile);
        try {
            const response = await sendOtp.send(mobile, msg91Conf.MSG91_SENDER_ID);
            console.log(response);
            if (response.type === 'success') {
                console.log('OTP code sent');
                return {
                    status: true,
                    data: response
                }
            }

            console.log('Failed to sent OTP');
            return {
                status: false,
                data: response
            }
        } catch (error) {
            console.error(error);
            console.log('[==== Send OTP Error ====]',error);
            return {
                status: false,
                data: []
            }
        }
    },

    //Service for verify OTP for mobile app
    verifyOtpSentToMobile: async (mobile, otp) => {
        console.log('[==== Verify OTP Request ====]',mobile,otp);
        try {
            const response = await sendOtp.verify(mobile, otp);
            console.log(response);
            if (response.type === 'success') {
                console.log('OTP verified successfully');
                return {
                    status: true,
                    data: response
                }
            }

            console.log('Failed to verify OTP');
            return {
                status: false,
                data: response
            }
        } catch (error) {
            console.error(error);
            console.log('[==== Verify OTP Error ====]',error);
            return {
                status: false,
                data: []
            }
        }
    },

    //Service for resend the OTP for mobile app
    retryOtpSend: async (mobile) => {
        console.log('[==== Resend OTP Request ====]',mobile);
        try {
            const response = await sendOtp.retry(mobile, false);
            console.log(response);
            if (response.type === 'success') {
                console.log('OTP code sent again');
                return {
                    status: true,
                    data: response
                }
            }

            console.log('Failed to sent OTP again');
            return {
                status: false,
                data: response
            }
        } catch (error) {
            console.error(error);
            console.log('[==== Resend OTP Error ====]',error);
            return {
                status: false,
                data: []
            }
        }
    },

    //Service for updating bda employee profile
    updateBdaEmpProfile:async(input) => {
        console.log('[==== Web Edit Profile Request ====]',input);
        input.updated_on = new Date();
        try {
            const update = await db.one('UPDATE BDA_EMPLOYEE_PROFILE SET EMP_CODE = $1, FNAME = $2, LNAME = $3, EMAIL = $4, CONTACT_NO = $5, UPDATED_BY = $6, UPDATED_ON = $7 WHERE USER_ID = $8 RETURNING USER_ID',[input.emp_code,input.fname,input.lname,input.email,input.contact_no,input.user_id,new Date(),input.user_id])
            if(!update) return {
                status: false,
                message:'Please try again'
            }
            return {
                status: true,
                data:update.user_id,
                message:'Profile updated successfully!!'
            }
        } catch (error) {
            console.log('[==== Web Edit Profile Error ====]',error);
            return {
                status: false,
                message:'Internal error'
            }
        }
    },

    //Service for change password for bda employee
    newPasswordBDAEmp: async (input) => {
        console.log('[==== Web Change Password Request ====]',input);
        try {
            let oldPassword = await db.one('SELECT USER_PASSWORD FROM BDA_EMPLOYEE_PROFILE WHERE USER_ID = $1 AND STATUS = $2',[input.user_id,'Y'])
            if(!oldPassword) return {
                status: false,
                message: 'Please try again'
            }

            let isValidPassword = await passwordHandler.comparePassword(input.user_password, oldPassword.user_password);
            if(!isValidPassword) return {
                status: false,
                message: 'InvalidOldPassword'
            }

            if(input.new_password != input.confirm_password) return {
                status: false,
                message: 'PasswordMisMatch'
            }

            let newPassword = await passwordHandler.encodePassword(input.confirm_password);
            if(!newPassword) return {
                status: false,
                message:'Please try again'
            }

            let insertNewPassword = await db.one('UPDATE BDA_EMPLOYEE_PROFILE SET USER_PASSWORD = $1, UPDATED_ON = $2, UPDATED_BY = $3 WHERE USER_ID = $4 RETURNING USER_ID',[newPassword,new Date(),input.user_id,input.user_id])
            if(!insertNewPassword) return {
                status: false,
                message: 'Failed to change password'
            }

            return {
                status: true,
                data:insertNewPassword,
                message:'PasswordChanged'
            }

        } catch (error) {
            console.log('[==== Web Change Password Error ====]',input);
            return {
                status: false,
                message:'InternalError'
            }
        }
    },

    //Get all bda employee list
    getAllBDAEmployees:async () => {
        try {
            let bdaEmployee = await db.any('SELECT A.USER_ID,A.EMP_CODE,A.FNAME,A.LNAME,A.EMAIL,A.CONTACT_NO,A.USER_ROLE,A.STATUS,A.CREATED_ON,B.FNAME,B.LNAME FROM BDA_EMPLOYEE_PROFILE A LEFT OUTER JOIN(SELECT CREATED_BY,FNAME,LNAME FROM BDA_EMPLOYEE_PROFILE)B ON A.USER_ID = B.CREATED_BY')
            if(!bdaEmployee) return {
                status:false,
                message:'NoRecordFound'
            }
            return {
                status:true,
                data:bdaEmployee,
                message: 'Records fetched successfully'
            }
        } catch (error) {
            console.log('getAllBDAEmployees ERR', error)
            return {
                status: false,
                message: 'InternalError'
            }
        }
    },

    //Check duplicate email count web-portal
    checkUserExistOnRegister:async (user) => {
        try {
            let checkUser = await db.one('SELECT COUNT(*) AS USER_CNT FROM BDA_USER_REGISTRATION WHERE EMAIL = $1',[user.email.toLowerCase()])
            if(checkUser.user_cnt > 0) return {
                status: false,
                message: 'UserAlreadyExist'
            }
            return {
                status: true,
                data:checkUser.user_cnt,
                message: 'NewUser'
            }
        } catch (error) {
            return {
                status: false,
                message: 'InternalError'
            }
        }
    },

    //Forgot password for bda employee
    forgotPasswordBDA:async(user) => {
        console.log('[========== Forgot password for bda Request ========]',user)
        try {
            let emailCount = await db.one('SELECT COUNT(*) AS USER_CNT FROM BDA_EMPLOYEE_PROFILE WHERE EMAIL = $1',[user.email.toLowerCase()])
            if(emailCount.user_cnt <= 0) return {
                status: false,
                message: 'NoEmailFound'
            }

            let userInfo = await db.one('SELECT USER_ID,FNAME,LNAME FROM BDA_EMPLOYEE_PROFILE WHERE EMAIL = $1',[user.email.toLowerCase()])
            if(!userInfo) return {
                status: false,
                message: 'NoRecordFound'
            }

            let resetToken = await jwtHandler.resetPasswordToken(userInfo.user_id)
            if(!resetToken) return {
                status: false,
                message: 'ResetTokenErr'
            }

            let emailObj = {
                username:`${userInfo.fname} ${userInfo.lname}`,
                email:user.email,
                reset_token:resetToken
            }

            let sendEmail = await SendEmail.sendresetMail(emailObj);
            if(!sendEmail) return {
                status: false,
                message: 'FailedToSendMail'
            }

            return {
                status: true,
                data:sendEmail,
                message:'ResetLinkSent'
            }
        } catch (error) {
            console.log('[========= Forgot password for bda Error ===========]',error)
            return {
                status: false,
                message:'InternalError'
            }
            
        }
    },

    //BDA Reset Password Function
    BdaResetPasswordLink:async(user) => {
        console.log('[========== Reset Password Link for BDA ========]',user)
        try {
            if(user.new_password != user.confirm_password){
                return {
                    status: false,
                    message: 'PasswordMismatch'
                }
            }else{
                let encryptedPassword = await passwordHandler.encodePassword(user.confirm_password)
                if(!encryptedPassword){
                    return {
                        status: false,
                        message: 'EncryptionError'
                    }
                }else{
                    const encryptedToken = user.token.split(' ')
                    let decodedToken = await jwtHandler.verifyToken(encryptedToken[0])
                    console.log('[============= Token Information =============]')
                    console.log('encryptedToken ==>',encryptedToken)
                    console.log('decodedToken ===>',decodedToken)
                    console.log('[============= Token Ends Here ===============]')
                    if(!decodedToken){
                        return {
                            status: false,
                            message: 'TokenErr'
                        }
                    }else{
                        let updateNewPassword = await db.one('UPDATE BDA_EMPLOYEE_PROFILE SET USER_PASSWORD = $1 WHERE USER_ID = $2 RETURNING USER_ID',[encryptedPassword,decodedToken.data])
                        if(!updateNewPassword){
                            return {
                                status: false,
                                message: 'Failed'
                            }
                        }else{
                            return {
                                status: true,
                                data:updateNewPassword,
                                message:'PasswordChanged'
                            }
                        }
                    }
                }
            }
        } catch (error) {
            console.log('[========== Reset Password Link for BDA Err ========]',error)
            return {
                status: false,
                message: 'InternalServerError'
            }
        }
    },

    //Forgot password for institution
    forgotPasswordInstitution:async(user) => {
        console.log('[========= Forgot password for institution ===========]',user)
        try {
            let emailCount = await db.one('SELECT COUNT(*) AS USER_CNT FROM BDA_USER_PROFILE WHERE EMAIL = $1',[user.email.toLowerCase()])
            if(emailCount.user_cnt <= 0) return {
                status: false,
                message: 'NoEmailFound'
            }

            let userInfo = await db.one('SELECT USER_ID,COMPANY_NAME FROM BDA_USER_PROFILE WHERE EMAIL = $1',[user.email.toLowerCase()])
            if(!userInfo) return {
                status: false,
                message: 'NoRecordFound'
            }

            let resetToken = await jwtHandler.resetPasswordToken(userInfo.user_id)
            if(!resetToken) return {
                status: false,
                message: 'ResetTokenErr'
            }

            let sendEmail = await SendEmail.sendresetMailInstitution(userInfo.company_name,user.email,resetToken);
            if(!sendEmail) return {
                status: false,
                message: 'FailedToSendMail'
            }

            return {
                status: true,
                data:sendEmail,
                message:'ResetLinkSent'
            }
        } catch (error) {
            console.log('[========= Forgot password for institution Error ===========]',error)
            return {
                status: false,
                message:'InternalError'
            }
        }
    },

    //Institution Reset Password Function
    InstitutionResetPasswordLink:async(user) => {
        console.log('[========== Reset Password Link for Institution ========]',user)
        try {
            if(user.new_password != user.confirm_password){
                return {
                    status: false,
                    message: 'PasswordMismatch'
                }
            }else{
                let encryptedPassword = await passwordHandler.encodePassword(user.confirm_password)
                if(!encryptedPassword){
                    return {
                        status: false,
                        message: 'EncryptionError'
                    }
                }else{
                    const encryptedToken = user.token.split(' ')
                    let decodedToken = await jwtHandler.verifyToken(encryptedToken[0])
                    console.log('[============= Token Information =============]')
                    console.log('encryptedToken ==>',encryptedToken)
                    console.log('decodedToken ===>',decodedToken)
                    console.log('[============= Token Ends Here ===============]')
                    if(!decodedToken){
                        return {
                            status: false,
                            message: 'TokenErr'
                        }
                    }else{
                        let updateNewPassword = await db.one('UPDATE BDA_USER_PROFILE SET USER_PASSWORD = $1 WHERE USER_ID = $2 RETURNING USER_ID',[encryptedPassword,decodedToken.data])
                        if(!updateNewPassword){
                            return {
                                status: false,
                                message: 'Failed'
                            }
                        }else{
                            return {
                                status: true,
                                data:updateNewPassword,
                                message:'PasswordChanged'
                            }
                        }

                    }
                }
            }
        } catch (error) {
            console.log('[========== Reset Password Link for Institution Err ========]',error)
            return {
                status: false,
                message: 'InternalServerError'
            }

        }

    }
}