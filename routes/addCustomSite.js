var express = require('express');
var router = express.Router();
const db = require('../config/db').db;

//Inst Web-App
router.post('/getAllSites', (req,res)=>{
	console.log(req.body.user_id);
	db.any('SELECT A.SITE_ID,A.SITE_NAME,A.CA_SITE_NO,A.SITE_LAYOUT_NAME,A.SITE_DIMENSION,A.PURPOSE,A.STATUS,A.CREATED_ON,A.UPDATED_ON,A.REGISTRATION_DOC,A.ORIGINAL_REGISTRATION_DOC,A.REGISTRATION_NO,A.REMARK,B.FNAME AS APPROVER_FNAME,B.LNAME AS APPROVER_LNAME FROM BDA_USER_ORG_MAPPING A LEFT OUTER JOIN (SELECT FNAME,LNAME,USER_ID FROM BDA_EMPLOYEE_PROFILE)B ON A.UPDATED_BY = B.USER_ID  WHERE A.USER_ID=$1 AND A.SITE_FLAG = $2 ORDER BY CREATED_ON DESC',[req.body.user_id,'C'])
	.then((data)=>{
		console.log(data);
		res.status(200).send(data);
	}).catch((err)=>{
		console.log(err);
		res.send(err);
	})
})

//BDA Web-App
router.get('/getPendingSites', (req,res)=>{
	db.any('SELECT A.*,B.COMPANY_NAME,B.CONTACT_NO,B.EMAIL,B.COMPANY_ADDRESS FROM BDA_USER_ORG_MAPPING A INNER JOIN BDA_USER_PROFILE B ON A.CREATED_BY = B.USER_ID WHERE A.STATUS = $1 AND A.SITE_FLAG = $2',['N','C'])
	.then((data)=>{
		console.log(data);
		res.status(200).send(data);
	}).catch((err)=>{
		console.log(err);
		res.send(err);
	})
})

//BDA Web-App
router.get('/getApprovedSites', (req,res)=>{
	db.any('SELECT A.COMPANY_NAME,A.CONTACT_NO,A.EMAIL,A.COMPANY_ADDRESS,B.*,C.* FROM BDA_USER_PROFILE A INNER JOIN BDA_USER_ORG_MAPPING B ON A.user_id = b.USER_ID inner join bda_custom_site_processing c on b.site_id = c.site_id WHERE b.STATUS = $1',['Y'])
	.then((data)=>{
		console.log(data);
		res.status(200).send(data);
	}).catch((err)=>{
		console.log(err);
		res.send(err);
	})
})

//Instution web_app 
router.post('/create', (req,res)=>{
	console.log(req.body);
    let date = new Date();
    let h = `${date.getHours()}`.padStart(2, '0')
    let m = `${date.getMinutes()}`.padStart(2, '0')
    let s = `${date.getSeconds()}`.padStart(2, '0')
    let registration_no = `${date.getFullYear()}${date.getMonth() + 1}${date.getDate()}${h}${m}${s}`
    db.one("SELECT NEXTVAL('BDA_SITE_ID') AS new_site_id")
    .then((data)=>{
        db.none('INSERT INTO BDA_USER_ORG_MAPPING(USER_ID,SITE_ID,SITE_NAME,CA_SITE_NO,SITE_LAYOUT_NAME,SITE_DIMENSION,PURPOSE,STATUS,CREATED_ON, CREATED_BY,UPDATED_ON,UPDATED_BY,SITE_FLAG,REGISTRATION_DOC,ORIGINAL_REGISTRATION_DOC,REGISTRATION_NO) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16)',[req.body.user_id,data.new_site_id,req.body.site_name,req.body.site_no,req.body.layout_name,req.body.dimension,req.body.purpose,'N',date,req.body.user_id,null,null,'C',null,null,registration_no])
        .then(data => {
            res.status(200).send({'message':'success'})
        })
        .catch(err => {
            console.log(err)
            res.status(200).send({'message':'failed'})
        })
        
    })
    .catch(error => {
        console.log(error)
        res.status(200).send({'message':'InternalServerError'})
    })
})

//BDA Web_app
router.post('/insertCustomSiteDetails', (req,res)=>{
	console.log('----------------------------------------',req.body);
	var date = new Date();

	db.tx(t => {
		const q1 = t.none('INSERT INTO BDA_CUSTOM_SITE_PROCESSING(SITE_ID,COMP_FILE_NO,DIV_NAME,LAYOUT_NAME,SUB_LAYOUT_NAME,CA_SITE_NO,ALLOTTEE_ORGANISATION_NAME,DIM_EW,DIM_NS, DIM_TOTAL, DIM_TYPE,EAST,WEST,NORTH,SOUTH,PURPOSE_NAME,DATE_OF_LEASE_AGREEMENT,LEASE_PERIOD,NEXT_LEASE_RENEWAL_DATE,LUMPSUM_ANNUITY,AMOUNT,LEASE_REGN_NO,LEASE_REGN_DATE,BOARD_RES_NUMBER, BOARD_RES_DATE,COURT_NAME,CASE_TYPE,CASE_NO,CASE_YEAR,LEASE_EXTENSION_PERIOD,LEASE_RENEWAL_COMMENCING_FROM,LEASE_RENEWAL_REG_NO,LEASE_RENEWAL_REG_DATE,RENEWAL_ORDER_NO,RENEWAL_ORDER_DATE,LUMPSUM_ANNUITY_RENEWAL,RENEWAL_LEASE_AMOUNT,CHALLAN_NO,CHALLAN_DATE,REMARKS, STATUS, CREATED_ON, CREATED_BY,SITE_FLAG) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35,$36,$37,$38,$39,$40,$41,$42,$43,$44)', [req.body.site_id,req.body.comp_file_no,req.body.div_name,req.body.layout_name,req.body.sub_layout_name,req.body.site_number,req.body.site_name,req.body.dim_ew,req.body.dim_ns,req.body.dim_total,req.body.dim_type,req.body.east,req.body.west,req.body.north,req.body.south,req.body.purpose_name,req.body.date_of_lease_agreement, req.body.lease_period,req.body.next_lease_renewal_date,req.body.lumpsum_annuity,req.body.amount,req.body.lease_regn_no,req.body.lease_regn_date,req.body.board_res_number,req.body.board_res_date,req.body.court_name,req.body.case_type,req.body.case_no,req.body.case_year,req.body.lease_extension_period,req.body.lease_renewal_commencing_from,req.body.lease_renewal_reg_no,req.body.lease_renewal_reg_date,req.body.renewal_order_no,req.body.renewal_order_date,req.body.lumpsum_annuity_renewal,req.body.renewal_lease_amount,req.body.challan_no,req.body.challan_date,req.body.remarks,'Y',date,req.body.user_id,req.body.site_flag]);

		const q2 = t.none('INSERT INTO BDA_SITE_HIERARCHY_MASTER(SITE_ID,COMP_FILE_NO,DIV_NAME,LAYOUT_NAME,SUB_LAYOUT_NAME,CA_SITE_NO,ALLOTTEE_ORGANISATION_NAME,DIM_EW,DIM_NS, DIM_TOTAL, DIM_TYPE,EAST,WEST,NORTH,SOUTH,PURPOSE_NAME,DATE_OF_LEASE_AGREEMENT,LEASE_PERIOD,NEXT_LEASE_RENEWAL_DATE,LUMPSUM_ANNUITY,AMOUNT,LEASE_REGN_NO,LEASE_REGN_DATE,BOARD_RES_NUMBER, BOARD_RES_DATE,COURT_NAME,CASE_TYPE,CASE_NO,CASE_YEAR,LEASE_EXTENSION_PERIOD,LEASE_RENEWAL_COMMENCING_FROM,LEASE_RENEWAL_REG_NO,LEASE_RENEWAL_REG_DATE,RENEWAL_ORDER_NO,RENEWAL_ORDER_DATE,LUMPSUM_ANNUITY_RENEWAL,RENEWAL_LEASE_AMOUNT,CHALLAN_NO,CHALLAN_DATE,REMARKS, STATUS, CREATED_ON,CREATED_BY,SITE_FLAG) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35,$36,$37,$38,$39,$40,$41,$42,$43,$44)', [req.body.site_id,req.body.comp_file_no,req.body.div_name,req.body.layout_name,req.body.sub_layout_name,req.body.site_number,req.body.site_name,req.body.dim_ew,req.body.dim_ns,req.body.dim_total,req.body.dim_type,req.body.east,req.body.west,req.body.north,req.body.south,req.body.purpose_name,req.body.date_of_lease_agreement, req.body.lease_period,req.body.next_lease_renewal_date,req.body.lumpsum_annuity,req.body.amount,req.body.lease_regn_no,req.body.lease_regn_date,req.body.board_res_number,req.body.board_res_date,req.body.court_name,req.body.case_type,req.body.case_no,req.body.case_year,req.body.lease_extension_period,req.body.lease_renewal_commencing_from,req.body.lease_renewal_reg_no,req.body.lease_renewal_reg_date,req.body.renewal_order_no,req.body.renewal_order_date,req.body.lumpsum_annuity_renewal,req.body.renewal_lease_amount,req.body.challan_no,req.body.challan_date,req.body.remarks,'Y',date,req.body.user_id,'C']);

		const q3 = t.none('UPDATE BDA_USER_ORG_MAPPING SET CA_SITE_NO = $1,SITE_LAYOUT_NAME = $2,SITE_DIMENSION = $3,PURPOSE = $4,STATUS = $5,UPDATED_ON = $6,UPDATED_BY = $7 WHERE REGISTRATION_NO = $8 AND USER_ID = $9', [req.body.site_number,req.body.layout_name,req.body.dim_total,req.body.purpose_name,'Y',date,req.body.user_id,req.body.registration_no,req.body.institution_uid])

		return ([q1,q2,q3])
	})
	.then((data)=>{
		console.log(data);
		res.status(200).send({'message':'success'});
	}).catch((err)=>{
		console.log(err);
		res.send(err);
	})
})

module.exports = router;