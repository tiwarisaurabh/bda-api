var express = require('express');
const Dashboard = require('../controllers/dashboardInfo');
var router = express.Router();

//Total number of site web-portal
router.get('/total-site', async (req, res, next) => {
    let totalSites = await Dashboard.totalSites();
    if(!totalSites.status) res.status(401).json(totalSites)
    res.status(200).json(totalSites)
})

//Total number of registered/unregistered company
router.get('/total-company',async (req, res, next) => {
    let totalCompany = await Dashboard.totalCompany();
    if(!totalCompany.status) res.status(401).json(totalCompany)
    res.status(200).json(totalCompany)
})

//Total number of BDA Employee
router.get('/total-bda-employee', async (req, res, next) => {
    let bdaTotalEmp = await Dashboard.bdaEmployee();
    if (!bdaTotalEmp.status) res.status(401).json(bdaTotalEmp)
    res.status(200).json(bdaTotalEmp)
})

//Total number of Institution site
router.post('/total-institution-site', async (req, res, next) => {
    let institutionSite = await Dashboard._totalInstitutionSite(req.body);
    if (!institutionSite.status) res.status(201).json(institutionSite)
    res.status(200).json(institutionSite)
})

module.exports = router;