var express = require('express');
const User = require('../controllers/login')
var router = express.Router();

//Route for user registration for mobile app
router.post('/register', async (req, res) => {

  let user = await User.register(req.body);

  if (!user.status) res.status(201).json(user)

  res.status(200).json(user)
});

//Route for user login for mobile app
router.post('/login', async (req, res) => {
  let user = await User.login(req.body);

  if (!user.status) res.status(201).json(user)

  res.status(200).json(user)
});

//Route for generating form_id for mobile app
router.get('/get-user-id', async (req, res) => {
  let user = await User.generateUserID();
  if (!user.status) res.status(201).json(user)

  res.status(200).json(user)
});

//Route for employee registration for web-portal
router.post('/register-employee', async (req, res) => {
  let user = await User.registerEmployee(req.body);

  if (!user.status) res.status(401).json(user)

  res.status(200).json(user)
});

//Route for employee login for web-portal
router.post('/login-employee', async (req, res) => {
  let user = await User.loginEmployee(req.body);

  if (!user.status) res.status(401).json(user)

  res.status(200).json(user)
});

//Route for sending OTP for mobile app
router.post('/send-otp', async (req, res) => {
  let user = await User.sendOTP(req.body.phoneNumber);

  if (!user.status) res.status(201).json(user)

  res.status(200).json(user)
});

//Route for resending the OTP for mobile app
router.post('/retry-otp', async (req, res) => {
  let user = await User.retryOTP(req.body.phoneNumber);

  if (!user.status) res.status(201).json(user)

  res.status(200).json(user)
});

//Route for verifying the OTP for mobile app
router.post('/verify-otp', async (req, res) => {
  let user = await User.verifyOTP(req.body.phoneNumber, req.body.otp);

  if (!user.status) res.status(201).json(user)

  res.status(200).json(user)
});

//Route for updating bda employee profile
router.post('/update-bda-employee-profile', async (req, res) => {
  let update = await User.editBdaEmpProfile(req.body);
  if (!update.status) res.status(401).json(update)
  res.status(200).json(update)
})

//Router for change password bda employee
router.post('/change-password-bda-employee', async (req, res) => {
  let updatePassword = await User.changePasswordBDAEmp(req.body);
  if (!updatePassword.status) res.status(401).json(updatePassword)
  res.status(200).json(updatePassword)
})

//Router for get bda employee list
router.get('/get-all-bda-employee', async (req, res) => {
  let getEmployee = await User.bdaEmployeeList();
  if (!getEmployee.status) res.status(401).json(getEmployee)
  res.status(200).json(getEmployee)
})

//Router for verify duplicate email
router.post('/email-check', async (req, res) => {
  let user = await User.checkUser(req.body);
  if (!user) res.status(201).json(user)
  res.status(200).json(user)
})

//for institution forgot password
router.post('/forgot-password-institution', async (req, res) => {
  let user = await User._forgotPasswordInstitution(req.body)
  if (!user) res.status(201).json(user)
  res.status(200).json(user)
})

//reset password for bda employee
router.post('/reset-password-institution', async (req, res) => {
  let user = await User._InstitutionResetPasswordLink(req.body)
  if (!user) res.status(201).json(user)
  res.status(200).json(user)
})


//For BDA Employee
router.post('/forgot-password', async (req, res) => {
  let user = await User._forgotPasswordBDA(req.body)
  if (!user) res.status(201).json(user)
  res.status(200).json(user)
});

//reset password for bda employee
router.post('/reset-password', async (req, res) => {
  let user = await User._BdaResetPasswordLink(req.body)
  if (!user) res.status(201).json(user)
  res.status(200).json(user)
})





module.exports = router;