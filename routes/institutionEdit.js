var express = require('express');
const InstitutionProfile = require('../controllers/institutionCtrl');
var router = express.Router();

//Institution change password
router.post('/change-password', async (req, res, next) => {
    let changePassword = await InstitutionProfile.changePassword(req.body)
    if(!changePassword.status) res.status(201).json(changePassword)
    res.status(200).json(changePassword)
})

//Institution update profile
router.post('/update-profile', async (req, res, next) => {
    let profile = await InstitutionProfile.updateProfile(req.body)
    if(!profile.status) res.status(201).json(profile)
    res.status(200).json(profile)
})

//View profile
router.post('/profile', async (req, res) => {
    let profile = await InstitutionProfile.viewProfile(req.body)
    if(!profile.status) res.status(201).json(profile)
    res.status(200).json(profile)
})

module.exports = router;
