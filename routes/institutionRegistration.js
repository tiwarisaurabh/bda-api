var express = require('express');
const InstitutionRegistrationCtrl = require('../controllers/institutionRegistration')
var router = express.Router();


//Upload registration certificate
router.post('/upload-registration-certificate', async (req, res) => {
    let upload = await InstitutionRegistrationCtrl._uploadRegistrationCertificate(req.body)
    if (!upload) res.status(201).json(upload)
    res.status(200).json(upload)
})

//Get Uploaded registration certificate
router.post('/get-upload-registration-certificate', async (req, res) => {
    let upload = await InstitutionRegistrationCtrl._getRegistrationUploadedFile(req.body)
    if (!upload) res.status(201).json(upload)
    res.status(200).json(upload)
})

router.post('/insert-registration-details', async (req, res) => {
    let registration = await InstitutionRegistrationCtrl._insertRegistrationDetailsTemp(req.body)
    if(!registration) res.status(201).json(registration)
    res.status(200).json(registration)
})

router.post('/register',async(req,res) => {
    let registration = await InstitutionRegistrationCtrl._institutionRegistration(req.body)
    if(!registration) res.status(201).json(registration)
    res.status(200).json(registration)
})

router.post('/check-email',async(req, res) => {
    let email = await InstitutionRegistrationCtrl._checkUserExist(req.body)
    if(!email) res.status(201).json(email)
    res.status(200).json(email)
})

router.post('/delete-document', async(req, res) => {
    let document = await InstitutionRegistrationCtrl._deleteUploadedCertificate(req.body)
    if(!document) res.status(201).json(document)
    res.status(200).json(document)
})

module.exports = router;