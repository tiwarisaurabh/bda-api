var express = require('express');
const NewSiteReqModule = require('../controllers/newSiteReq');
const { route } = require('./pendingUser');
var router = express.Router();

router.post('/new-site-request', async (req, res, next) => {
    let site = await NewSiteReqModule._addNewSite(req.body)
    if (!site) res.status(201).json(site)
    res.status(200).json(site)
})

router.post('/approve-new-site-request', async (req, res, next) => {
    let site = await NewSiteReqModule._approveNewSiteReq(req.body)
    if (!site) res.status(201).json(site)
    res.status(200).json(site)
})

router.get('/pending-new-site-request', async (req, res, next) => {
    let pendingSites = await NewSiteReqModule._getPendingNewSite()
    if(!pendingSites) res.status(201).json(pendingSites)
    res.status(200).json(pendingSites)
})

router.post('/reject-pending-new-site-request', async (req, res, next) => {
    let rejectSites = await NewSiteReqModule._rejectPendingNewSite(req.body)
    if(!rejectSites) res.status(201).json(rejectSites)
    res.status(200).json(rejectSites)
})

router.post('/store-new-site-req',async (req, res, next) => {
    let userSite = await NewSiteReqModule._storeNewSiteRequestAsTemp(req.body);
    if(!userSite) res.status(201).json(userSite)
    res.status(200).json(userSite)
})

router.post('/get-uploaded-document',async(req, res, next) => {
    let site = await NewSiteReqModule._getTempNewSiteReq(req.body);
    if(!site) res.status(201).json(site)
    res.status(200).json(site)
})

router.post('/upload-registration-certificate', async (req, res, next) => {
    let upload = await NewSiteReqModule._uploadRegistrationCertificate(req.body)
    if(!upload) res.status(201).json(upload)
    res.status(200).json(upload)
})

router.post('/register-new-site',async(req,res) => {
    let registration = await NewSiteReqModule._institutionRegistration(req.body)
    if(!registration) res.status(201).json(registration)
    res.status(200).json(registration)
})

router.post('/delete-document', async(req, res) => {
    let document = await NewSiteReqModule._deleteUploadedCertificate(req.body)
    if(!document) res.status(201).json(document)
    res.status(200).json(document)
})


module.exports = router;