var express = require('express');
const PendingUserModule = require('../controllers/pendingUser')
var router = express.Router();

//Route for pending user details for web-portal
router.get('/get-pending-user-list', async (req, res, next) => {
  let user = await PendingUserModule.pendingUsers(req.body);

  if (!user.status) res.status(401).json(user)

  res.status(200).json(user)
});

//Route for approve pending user for web-portal
router.post('/approve-pending-user',async (req, res, next) => {
  let user = await PendingUserModule._approveInstitutionRegistration(req.body);
  if(!user.status) res.status(401).json(user)
  res.status(200).json(user)
})

//Route for reject registration
router.post('/reject-pending-user', async (req, res, next) => {
  let user = await PendingUserModule._rejectInstitutionRegistration(req.body);
  if(!user.status) res.status(401).json(user)
  res.status(200).json(user)
})

//Route for get approved user details for web-portal
router.get('/get-approved-user-list',async(req,res, next) => {
  let user = await PendingUserModule.getAllApprovedUsers();
  if(!user.status) res.status(401).json(user)
  res.status(200).json(user)
})

//To check registration status
router.post('/registration-check',async (req, res, next) => {
  let userStatus = await PendingUserModule.registrationStatus(req.body);
  if(!userStatus.status) res.status(201).json(userStatus)
  res.status(200).json(userStatus)
})

module.exports = router;