var express = require('express');
const PurchaseOrder = require('../controllers/purchase-order')
var router = express.Router();

//Create purchase order for mobile app
router.post('/create-order', async (req, res, next) => {
    let purchase = await PurchaseOrder.generatePurchaseOrder(req.body);

    if (!purchase.status) res.status(201).json(purchase)

    res.status(200).json(purchase)
});

//Get Purchase order details for web-portal
router.get('/purchase-item', async (req, res) => {
    let purchase = await PurchaseOrder.getPurchaseItems();
    if(!purchase.status) res.status(401).json(purchase)

    res.status(200).json(purchase)
})

router.get('/pending-purchase-item', async (req, res) => {
    let purchase = await PurchaseOrder.getPendingPurchaseOrderDetails();
    if(!purchase.status) res.status(201).json(purchase)
    res.status(200).json(purchase)
})

//To get institution purchase order details
router.post('/institution-purchase-dtl', async (req, res, next) => {
    let purchase = await PurchaseOrder.institutionPurchaseDetails(req.body);
    if(!purchase.status) res.status(201).json(purchase)
    res.status(200).json(purchase)
})

//To upload verified purchase copy
router.post('/upload-signed-copy', async (req, res, next) => {
    console.log('req.body',req.body)
    let purchase = await PurchaseOrder.uploadSignedCopy(req.body);
    if(!purchase.status) res.status(201).json(purchase)
    res.status(200).json(purchase)
})

//Audit Purchase Details web-portal
router.post('/audit-purchase-order', async (req, res) => {
    let audit = await PurchaseOrder.purchaseOrderAudit(req.body);
    if(!audit.status) res.status(201).json(audit)
    res.status(200).json(audit)
})

//Update existing purchase order
router.post('/update-purchase-order', async (req, res) => {
    let audit = await PurchaseOrder.updatePurchaseReq(req.body);
    if(!audit.status) res.status(201).json(audit)
    res.status(200).json(audit)
})

//Purchase timline history
router.post('/institution-purchase-history',async (req,res) => {
    let history = await PurchaseOrder.institutionPurchaseTimline(req.body);
    if(!history) res.status(201).json(history)
    res.status(200).json(history)
})

//Purchase timline history for bda-web-portal
router.post('/purchase-history',async (req, res) => {
    let history = await PurchaseOrder.purchaseTimline(req.body);
    if(!history) res.status(201).json(history)
    res.status(200).json(history)
})

//Institution pending purchase order for institution web-portal
router.post('/pending-institution-purchase', async (req, res) => {
    let pendingPurchaseInstitution = await PurchaseOrder.pendingInstitutionPurchaseList(req.body);
    if(!pendingPurchaseInstitution) res.status(201).json(pendingPurchaseInstitution)
    res.status(200).json(pendingPurchaseInstitution)
})

router.post('/order-dtl', async (req, res) => {
    let order = await PurchaseOrder.purchaseOrderDtlMobile(req.body);
    if(!order) return res.status(201).json(order)
    res.status(200).json(order)
})


module.exports = router;