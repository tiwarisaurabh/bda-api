var express = require('express');
var router = express.Router();
var xl = require('excel4node');
const db = require('../config/db').db;
const fs = require('fs');
const path = require('../config/constant');

//BDA Purpose Name
router.get('/purposeName', (req, res) => {
    db.any('SELECT DISTINCT PURPOSE_NAME FROM BDA_SITE_HIERARCHY_MASTER')
        .then((data) => {
            res.status(200).send(data);
        }).catch((err) => {
            console.log(err)
            res.status(200).send({
                'message': 'No Data Found'
            });
        })
})

router.get('/divName', (req, res) => {
    db.any('SELECT DISTINCT DIV_NAME FROM BDA_SITE_HIERARCHY_MASTER')
        .then((data) => {
            res.status(200).send(data);
        }).catch((err) => {
            console.log(err)
            res.status(200).send({
                'message': 'No Data Found'
            });
        })
})

//BDA Report
router.post('/bdaSiteReport', (req, res) => {
    console.log('***************',req.body)
    var purpose_name = [],
        div_name = [],
        where, values;
    var purpose_items = req.body.purpose_name;
    var div_items = req.body.div_name;
    var status = req.body.status;
    var range_from = req.body.dim_from;
    var range_to = req.body.dim_to;

    if (purpose_items.length != 0) {
        purpose_items.forEach(item => purpose_name.push(item.purpose_name));
    }
    if (div_items.length != 0) {
        div_items.forEach(item => div_name.push(item.div_name));
    }

    if (purpose_name == '' && div_name == '' && req.body.dim_from == '' && status == '') {
        res.status(200).send({
            'message': 'nullInput'
        });

    } else if (purpose_name.length != 0 && div_name == '' && range_from == '' && status == '') {
        where = 'purpose_name like any ($1)';
        values = [purpose_name];
        process();
    } else if (purpose_name.length != 0 && div_name.length != 0 && range_from == '' && status == '') {
        where = 'purpose_name like any ($1) and div_name like any ($2)';
        values = [purpose_name, div_name];
        process();
    } else if (purpose_name != null && div_name != null && range_from != null && status == '') {
        where = 'purpose_name like any ($1) and div_name like any ($2) and dim_total BETWEEN $3 AND $4';
        values = [purpose_name, div_name, range_from, range_to];
        process();
    } else if (purpose_name == '' && div_name == '' && range_from == '' && status != null) {
        where = 'custom_site_flag = $1';
        values = [status.id];
        process();
    } else if (purpose_name == '' && div_name == '' && range_from != null && status.id != null) {
        where = 'dim_total BETWEEN $1 AND $2 and custom_site_flag =$3';
        values = [range_from, range_to, status.id];
        process();
    } else if (purpose_name == '' && div_name != null && range_from != null && status != null) {
        where = 'div_name like any ($1) and dim_total BETWEEN $2 AND $3 and custom_site_flag =$4';
        values = [div_name, range_from, range_to, status.id];
        process();
    } else if (purpose_name != null && div_name == '' && range_from == '' && status != null) {
        where = 'purpose_name like any ($1) and custom_site_flag =$2';
        values = [purpose_name, status.id];
        process();
    } else if (purpose_name == '' && div_name != null && range_from != null && status == '') {
        where = 'div_name like any ($1) and dim_total BETWEEN $2 AND $3';
        values = [div_name, range_from, range_to];
        process();
    } else if (purpose_name != null && div_name == '' && range_from != null && status == '') {
        where = 'purpose_name like any ($1) and dim_total BETWEEN $2 AND $3';
        values = [purpose_name, range_from, range_to];
        process();
    } else if (purpose_name == '' && div_name != null && range_from == '' && status != null) {
        where = 'dim_total BETWEEN $1 AND $2 and custom_site_flag =$3';
        values = [range_from, range_to, status.id];
        process();
    } else if (purpose_name != null && div_name == '' && range_from != null && status != null) {
        where = 'purpose_name like any ($1) and dim_total BETWEEN $2 AND $3 and custom_site_flag =$4'
        values = [purpose_name, range_from, range_to, status.id];
        process();
    } else if (purpose_name != null && div_name != null && range_from == '' && status != null) {
        where = 'purpose_name like any ($1) and div_name like any ($2) and custom_site_flag =$3';
        values = [purpose_name, div_name, status.id];
        process();
    } else if (purpose_name == '' && div_name != null && range_from == '' && status == '') {
        where = 'div_name like any ($1)';
        values = [div_name];
        process();
    } else if (purpose_name == '' && div_name == '' && range_from != null && status == '') {
        where = 'dim_total BETWEEN $1 AND $2';
        values = [range_from, range_to];
        process();
    } else if (purpose_name != null && div_name != null && range_from != null && status != null) {
        where = 'purpose_name like any ($1) and div_name like any ($2) and dim_total BETWEEN $3 AND $4 and custom_site_flag = $5';
        values = [purpose_name, div_name, range_from, range_to, status.id];
        process();
    } else {
        console.log("Something went wrong!");
        res.status(200).send({
            'message': 'Something went wrong!'
        });
    }

    function process() {
        db.any('select * from bda_site_hierarchy_master where ' + where, values)
            .then((data) => {
                generateExcel(data);
            }).catch((err) => {
                console.log(err)
                res.status(200).send({
                    'message': 'No Data Found'
                });
            })
    }

    function generateExcel(data) {
        var wb = new xl.Workbook();
        var ws = wb.addWorksheet('BDA Site Report', {
            disableRowSpansOptimization: true
        });

        ws.row(1).setHeight(40);
        ws.row(2).setHeight(30);
        ws.column(1).setWidth(1).hide();
        ws.column(2).setWidth(5);
        ws.column(3).setWidth(15);
        ws.column(4).setWidth(15);
        ws.column(5).setWidth(15);
        ws.column(6).setWidth(20);
        ws.column(7).setWidth(15);
        ws.column(8).setWidth(40);
        ws.column(9).setWidth(12);
        ws.column(10).setWidth(12);
        ws.column(11).setWidth(12);
        ws.column(12).setWidth(15);
        ws.column(13).setWidth(15);
        ws.column(14).setWidth(15);
        ws.column(15).setWidth(15);
        ws.column(16).setWidth(15);
        ws.column(17).setWidth(20);
        ws.column(18).setWidth(30);
        ws.column(19).setWidth(23);
        ws.column(20).setWidth(26);
        ws.column(21).setWidth(22);
        ws.column(22).setWidth(20);
        ws.column(23).setWidth(20);
        ws.column(24).setWidth(20);
        ws.column(25).setWidth(20);
        ws.column(26).setWidth(20);
        ws.column(27).setWidth(25);
        ws.column(28).setWidth(15);
        ws.column(29).setWidth(15);
        ws.column(30).setWidth(15);
        ws.column(31).setWidth(22);
        ws.column(32).setWidth(30);
        ws.column(33).setWidth(25);
        ws.column(34).setWidth(25);
        ws.column(35).setWidth(20);
        ws.column(36).setWidth(20);
        ws.column(37).setWidth(25);
        ws.column(38).setWidth(25);
        ws.column(39).setWidth(18);
        ws.column(40).setWidth(18);
        ws.column(41).setWidth(30);

        ws.cell(1, 2, 1, 41, true).string('BDA SITE REPORT').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: '000080',
                size: 16
            },
            border: {
                bottom: {
                    style: 'medium',
                    color: '000000'
                },
                right: {
                    style: 'medium',
                    color: '000000'
                },
                left: {
                    style: 'medium',
                    color: '000000'
                },
                top: {
                    style: 'medium',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '3465a4',
                fgColor: 'FFFFFF'
            }
        });
        ws.cell(2, 2).string('S.No.').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 3).string('COMP FILE NO').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 4).string('DIV NAME').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 5).string('LAYOUT NAME').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 6).string('SUB LAYOUT NAME').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 7).string('CA SITE NO').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 8).string('ALLOTTEE ORGANISATION NAME').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 9).string('DIM E-W').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 10).string('DIM N-S').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 11).string('DIM TOTAL').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['distributed']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 12).string('DIM TYPE').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 13).string('EAST').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 14).string('WEST').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 15).string('NORTH').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 16).string('SOUTH').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 17).string('PURPOSE_NAME').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 18).string('DATE OF LEASE AGREEMENT').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['distributed']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 19).string('LEASE PERIOD').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 20).string('NEXT LEASE RENEWAL DATE').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 21).string('LUMPSUM ANNUITY').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 22).string('AMOUNT').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 23).string('LEASE REGN NO').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 24).string('LEASE REGN DATE').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 25).string('BOARD RES NUMBER').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['distributed']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 26).string('BOARD RES DATE').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 27).string('COURT NAME').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 28).string('CASE TYPE').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 29).string('CASE NO').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 30).string('CASE YEAR').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 31).string('LEASE EXTENSION PERIOD').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 32).string('LEASE RENEWAL COMMENCING FROM').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 33).string('LEASE RENEWAL REG NO').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 34).string('LEASE RENEWAL REG DATE').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 35).string('RENEWAL ORDER NO').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['distributed']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 36).string('RENEWAL ORDER DATE').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 37).string('LUMPSUM ANNUITY').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 38).string('RENEWAL LEASE AMOUNT').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 39).string('CHALLAN NO').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 40).string('CHALLAN DATE').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });
        ws.cell(2, 41).string('REMARKS').style({
            alignment: {
                vertical: ['center'],
                horizontal: ['center']
            },
            font: {
                bold: true,
                color: 'FFFFFF',
                size: 12
            },
            border: {
                bottom: {
                    style: 'thin',
                    color: '000000'
                },
                right: {
                    style: 'thin',
                    color: '000000'
                },
                left: {
                    style: 'thin',
                    color: '000000'
                },
                top: {
                    style: 'thin',
                    color: '000000'
                }
            },
            fill: {
                type: 'pattern',
                patternType: 'darkDown',
                bgColor: '000080',
                fgColor: '000080'
            }
        });

        const startRow = 3;
        if (data.length) {
            data.forEach((item, i) => {
                const currentRow = i + startRow;
                ws.cell(currentRow, 2).number(i + 1).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 3).string(item.comp_file_no).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 4).string(item.div_name).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 5).string(item.layout_name).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 6).string(item.sub_layout_name).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 7).string(item.ca_site_no).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 8).string(item.allottee_organisation_name).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 9).string(item.dim_ew).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 10).string(item.dim_ns).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 11).string(item.dim_total).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 12).string(item.dim_type).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 13).string(item.east).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 14).string(item.west).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 15).string(item.north).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 16).string(item.south).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 17).string(item.purpose_name).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 18).string(item.date_of_lease_agreement).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 19).string(item.lease_period).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 20).string(item.next_lease_renewal_date).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 21).string(item.lumpsum_annuity).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 22).string(item.amount).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 23).string(item.lease_regn_no).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 24).string(item.lease_regn_date).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 25).string(item.board_res_number).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 26).date(item.board_res_date).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 27).string(item.court_name).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 28).string(item.case_type).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 29).string(item.case_no).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 30).string(item.case_year).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 31).string(item.lease_extension_period).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 32).string(item.lease_renewal_commencing_from).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 33).string(item.lease_renewal_reg_no).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 34).date(item.lease_renewal_reg_date).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 35).string(item.renewal_order_no).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 36).date(item.renewal_order_date).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 37).string(item.lumpsum_annuity_renewal).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 38).string(item.renewal_lease_amount).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 39).string(item.challan_no).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 40).string(item.challan_date).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
                ws.cell(currentRow, 41).string(item.remarks).style({
                    alignment: {
                        vertical: ['center'],
                        horizontal: ['left']
                    },
                    font: {
                        color: '000000',
                        size: 12
                    },
                    border: {
                        bottom: {
                            style: 'thin',
                            color: '000000'
                        },
                        right: {
                            style: 'thin',
                            color: '000000'
                        },
                        left: {
                            style: 'thin',
                            color: '000000'
                        },
                        top: {
                            style: 'thin',
                            color: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'darkDown',
                        bgColor: 'FFFFFF',
                        fgColor: 'FFFFFF'
                    }
                });
            })
            wb.write(path.identityPath + 'Bda_site_report.xlsx');
            res.status(200).send({
                'message': 'success'
            });
        } else {
            res.status(200).send({
                'message': 'No Data'
            });
            fs.stat(path.identityPath + filename + '.xlsx', function (err, stats) {
                console.log(stats);

                if (err) {
                    return console.error(err);
                }

                fs.unlink(path.identityPath + filename + '.xlsx', function (err) {
                    if (err) return console.log(err);
                    console.log('file deleted successfully');
                });
            });
        };
    }
})

module.exports = router;