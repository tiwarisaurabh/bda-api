var express = require('express');
const SiteModule = require('../controllers/siteInfo');
var router = express.Router();

//Route for get available site details for mobile app
router.get('/get-available-site', async (req, res, next) => {
  let site = await SiteModule.getSite();

  if (!site.status) res.status(201).json(site)

  res.status(200).json(site)
});

//Route for view site details and dimensions for mobile app
router.post('/view-site-summary', async (req, res, next) => {
  let site = await SiteModule.getSiteSummary(req.body);

  if (!site.status) res.status(201).json(site)

  res.status(200).json(site)
});

//Route for getting site_id and site_name list for mobile app
router.get('/get-organization',async (req,res,next) => {
  let site = await SiteModule.getAvailableSite();
  if(!site.status) res.status(201).json(site)
  res.status(200).json(site)
})

//Route for getting user site details for mobile app
router.post('/get-user-sites',async (req, res, next) => {
  let userSite = await SiteModule.userSiteDetails(req.body);
  if(!userSite) res.status(201).json(userSite)
  res.status(200).json(userSite)
})

//Route for getting list of site for user on institution side
router.post('/user-site-new-site-req',async (req, res, next) => {
  let userSite = await SiteModule._getSiteMasterDataForUser(req.body);
  if(!userSite) res.status(201).json(userSite)
  res.status(200).json(userSite)
})

//Route for getting list of site for user on institution side
router.post('/new-site-create-order',async (req, res, next) => {
  let userSite = await SiteModule._getNotScreeningPhaseSite(req.body);
  if(!userSite) res.status(201).json(userSite)
  res.status(200).json(userSite)
})







module.exports = router;


