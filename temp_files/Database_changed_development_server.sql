--created new tables for new site request 
CREATE TABLE bda_new_site_request 
(
    seq_id serial,
    user_id bigint,
    site_id bigint,
    site_name character varying,
    ca_site_no character varying,
    site_layout_name character varying,
    site_dimension character varying,
    purpose character varying,
    status character(1),
    created_on timestamp without time zone,
    created_by bigint,
    updated_on timestamp without time zone,
    updated_by bigint
);
