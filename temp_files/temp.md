#Challengage:
1. On CASite Officer - webbda.powerguru.co.in
	SideMenu-> 
		Registration-> 
			Approved

	Issue: Unable to map/get approved NEW_SITE_REQUEST rows here.

1. For approved site, 
	data is getting based on only if user is verified(status='Y') from bda_user_profile table, not maintaining based on CA-SITE individually.

	what happen if approver wants to approve only one CA-site.

2. You mean to say form_id = user_id = registration_id.
	And as far as I know, one entity can have multiple id_value but one id_value can't be assigned for multiple entity.

3. And where custom_site are getting created?

4. why user's informations(email,password...) stored in two tables bda_user_profile & bda_user_registration.

5. How can we restrict purchased CA site ?

1. For approved site,
      data is getting based on only if user is verified(status='Y') from bda_user_profile table, not maintaining based on CA-SITE individually.

      what happen if approver wants to approve only one CA-site.

Solution:- The site status is not being considered it is not being mentioned that the partial approval will be also their in order to approve a particular site.

	>You don't think NEW_SITE_REQUEST is the same situtation where we have to keep that info.

2. You mean to say form_id = user_id = registration_id.
      And as far as I know, one entity can have multiple id_value but one id_value can't be assigned for multiple entity.

Solution:- The user_id i.e form_id = user_id = registration_id. is  being mapped into the BDA_USER_ORG_MAPPING table with the site_id along with the meta-data.

	>In future, it'll be inaccessible to the lowest entity.

3. And where custom_site are getting created?

Solution:- Custom site is getting created from the same drop-down from where the user select the master site. Drop-down is bidirectional i.e select as well as enter.

	>Can't able to insert any value, all fields are inserted as null.

4. why user's informations(email,password...) stored in two tables bda_user_profile & bda_user_registration.

Solution:- In order to avoid subsequent update i.e to avoid table unlock 

	>It's confusing & data redundancy..

5. How can we restrict purchased CA site ?

Solution:- Drop-down should only reflect the site which are not being in purchase table 