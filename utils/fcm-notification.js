"use strict";

const FCM = require('fcm-node');
const fcmCredentials = require('../config/constant').fcmCredentials;

module.exports = {
    
    //Helper for sending notification messages
    sendNotification: async (notify) => {
        console.log("[==== FCM Request ====]",notify)
        return new Promise(async (resolve, reject) => {
            let fcm = new FCM(fcmCredentials.serverKey);
            let message = {
                to: notify.token,

                notification: {
                    title: notify.title,
                    body: notify.body
                },
            }

            fcm.send(message, function (err, response) {
                if (err) {
                    console.log("[=== FCM Error ===]",err)
                    resolve(err)
                } else {
                    console.log("[=== FCM Success Response ===]",response)
                    resolve(response)
                }
            })
        })
    },
}