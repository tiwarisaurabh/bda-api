"use strict";

module.exports = {

    //Helper for decoding base64 
    decodeBase64Image: async (dataString) => {
        // console.log('[=== Decode Base64 Image Request ===]',dataString)
        var baseContent = await dataString.replace(/^data:image\/\w+;base64,/, "");
        const base64Data = Buffer.from(baseContent, 'base64');
        const type = dataString.split(';')[0].split('/')[1];

        return {
            type: type,
            data: base64Data
        };
    },

    decodeBase64Pdf:async(dataString) => {
        const type = 'pdf';
        var baseContent = await dataString.split(';base64,').pop();
        const base64Data = Buffer.from(baseContent, 'base64');
        return {
            type: type,
            data: base64Data
        }
    }
}