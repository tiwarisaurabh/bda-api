const jwt = require('jsonwebtoken');
const secret = 'hdaiu$$^%67777siojvIIUfbvheiruejwrkekdmfvdnahsrfw8urewriwjUTYt$@#@R^T&T'

module.exports = {

    //Helper for login
    signToken: async (input) => {
        const token = await jwt.sign({
            data: input
        }, secret);
        return token;
    },
    
    //Helper for verify token
    verifyToken: async (token) => {
        try {
            var decoded = await jwt.verify(token, secret);
            if (decoded) {
                return decoded;
            } else {
                return null;
            }
        } catch (err) {
            console.log(err)
            return null;
        }
    },

    resetPasswordToken: async (user_id) => {
        const token = await jwt.sign({
            exp: Math.floor(Date.now() / 1000) + (60 * 60),
            data: user_id
        }, secret);
        return token;
    },
}