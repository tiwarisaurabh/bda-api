const bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(10);

module.exports = {

    //Helper for password encoding
    encodePassword: async (password) => {
        try {
            let hash = await bcrypt.hashSync(password, salt);
            return hash;
        } catch (error) {
            console.log(error);
            return null;
        }
    },

    //Helper for password decoding
    comparePassword: async (password, hash) => {
        try {
            let status = await bcrypt.compareSync(password, hash);
            return status;
        } catch (error) {
            console.log(error);
            return null;
        }
    }
}