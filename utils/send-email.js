"use strict";

const rp = require('request-promise');
const postmark_key = '3a1dc399-0e40-40ee-bee5-bafa30993241';

module.exports = {

    //Helper for sending Email
    sendMail: async (input) => {
        console.log('[=== Send Email Request ===]', input);
        return new Promise(async (resolve, reject) => {
            let options = {
                method: 'POST',
                uri: 'https://api.postmarkapp.com/email/withTemplate',
                headers: {
                    'content-type': 'application/json',
                    'X-Postmark-Server-Token': postmark_key
                },
                body: {
                    From: 'info@esuvidhamitra.com',
                    To: input.email,
                    TemplateId: input.template_id,
                    TemplateModel: {
                        person: {
                            name: input.company_name,
                            form_id: input.form_id
                        }
                    }
                },
                json: true
            }
            rp(options)
                .then(async (body) => {
                    resolve(body)
                })
                .catch(function (err) {
                    reject(err)
                });
        })
    },

    //Forgot Password for Institution
    sendresetMailInstitution: async (username,email,reset_token) => {
        // console.log('[===send reset mail====]',username,email,reset_token)
        return new Promise(async (resolve, reject) => {
            let options = {
                method: 'POST',
                uri: 'https://api.postmarkapp.com/email/withTemplate',
                headers: {
                    'content-type': 'application/json',
                    'X-Postmark-Server-Token': postmark_key
                },
                body: {
                    From: 'info@esuvidhamitra.com',
                    To: email,
                    TemplateId: 20193284,
                    TemplateModel: {
                        person: {
                            username: username,
                            link: `http://webinstitution.powerguru.co.in/#/resetpassword?${reset_token}`,             // link for production
                            //link:`http://localhost:4200/#/resetpassword?${reset_token}`                              // link for development
                        }
                    }
                },
                json: true
            }
            rp(options)
                .then(async (body) => {
                    resolve(body)
                })
                .catch(function (err) {
                    reject(err)
                });
        })
    },

    //Forgot Password for BDA
    sendresetMail: async (input) => {
         console.log('[===send reset mail====]',input)
        return new Promise(async (resolve, reject) => {
            let options = {
                method: 'POST',
                uri: 'https://api.postmarkapp.com/email/withTemplate',
                headers: {
                    'content-type': 'application/json',
                    'X-Postmark-Server-Token': postmark_key
                },
                body: {
                    From: 'info@esuvidhamitra.com',
                    To: input.email,
                    TemplateId: 21000181,
                    TemplateModel: {
                        person: {
                            username: input.username,
                            link: `http://webbda.powerguru.co.in/#/resetpassword?${input.reset_token}`,             // link for production
                            //link:`http://localhost:4200/#/resetpassword?${input.reset_token}`                   //Link for local testing
                        }
                    }
                },
                json: true
            }
            rp(options)
                .then(async (body) => {
                    resolve(body)
                })
                .catch(function (err) {
                    reject(err)
                });
        })
    }
}